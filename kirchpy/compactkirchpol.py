# -*- coding: utf-8 -*-
"""
© 2017, ETH Zurich, Pencho Yordanov

Change of variables representation of Kirchhoff polynomials.
"""

#from kirchexpr import *
import kirchpy.common as cmn
import networkx as nx

#TODO: error handling -- catch errors while manipulating expressions in this representation

class CompactKirchPol(object):
    """CompactKirchPol -- compact representation of a Kirchhoff polynomial using change of variables.

    Parameters
    ---------
    name:            name of the expression
    changedVarsDict: dictionary of variable:expression pairs denoting the changes of variables through an equation var=expr
    
    """
    def __init__(self, name=None, changedVarsDict={}):
        if name is not None:
            self.name = name
        else:
            self.name = 'Change of variables form'
        self.changedVarsDict = changedVarsDict
    
    
    def addEquation(self, variable, expression):
        """addEquation -- 
        """
        self.changedVarsDict[variable] = expression


    def removeEquation(self, variable):
        """removeEquation -- 
        """
        del self.changedVarsDict[variable]


    def getDictIterator(self):
        """getDictIterator --
        """
        return self.changedVarsDict.iteritems()


    def generateExpression(self, appearance='default', edgeAttribute=None, enumArbs=False,  heuristic=None, factorIdDict=None, monitorRecursion=False, showDigrsWithPossiblyEqualKirchPols=False, topologicalSort=True):
        """generateExpression -- generate expression in implicit form using the substitution of variables representation.
        """        
        inputVector   = [appearance, edgeAttribute, enumArbs, heuristic, factorIdDict, monitorRecursion,showDigrsWithPossiblyEqualKirchPols,topologicalSort]
        eqSeparator   = cmn.opDict[appearance]['eqSeparator']
        substOperator = cmn.opDict[appearance]['subst']
        
        if topologicalSort: # topological sort for the vertices of the assembly DAG; allows sequential evaluation of the substitutions (starting from the last equation).
            topSortedVertices = nx.topological_sort(self.getAssemblyDAG())
            return eqSeparator.join([str(k) + substOperator + self.changedVarsDict[k].generateExpression(*inputVector) for k in topSortedVertices])
        else:
            return eqSeparator.join([str(k) + substOperator + v.generateExpression(*inputVector) for k,v in self.getDictIterator()])


    def getAssemblyDAG(self):
        """getAssemblyDAG -- 
        """
        G = nx.DiGraph()
        
        if len(self.changedVarsDict)==1: # the assembly DAG consists of a single vertex
            G.add_nodes_from(self.changedVarsDict.keys())
        else:
            edgesDAG =cmn.flatten([[ (k,x)                   
                                     for x in v.getIndeterminates()
                                     if x.startswith(cmn.tokenName)]
                                         for k,v in self.getDictIterator()])
            G.add_edges_from(edgesDAG)
        return G


    def exprTreeStructureData(self):
        """exprTreeStructureData --
        """
        return {k:v.exprTreeStructureData() for k,v in self.getDictIterator()}
        
        
    def getCompressedLength(self):
        """ getCompressedLength -- use only when the Kirchhoff polynomials are fully enumerated. Size of each expression tree +1 for the pointer.
        """
        return sum([v.getCompressedLength() for k,v in self.getDictIterator()]) + len(self.changedVarsDict)


    def condenseOperationsInExpressionTree(self):
        """condenseOperationsInExpressionTree -- condense MUL([a,MUL([b,...])],...) to MUL([a,b,...,...) and ADD([a,ADD([b,...])],...) to ADD([a,b,...,...).
        """        
        self.changedVarsDict = {k:v.condenseOperationsInExpressionTree() for k,v in self.getDictIterator()}
        return self
