# -*- coding: utf-8 -*-
"""
© 2017, ETH Zurich, Pencho Yordanov
"""
import kirchpy.common as cmn
import kirchpy.compactkirchpol as cmpct

import networkx as nx
import networkx.algorithms.isomorphism as iso
import networkx.algorithms.dominance as dom

import ast
import sympy
import random
import itertools as it
import numpy as np
from itertools import groupby
from copy import deepcopy
from operator import itemgetter

#import matplotlib.pyplot as plt
#import matplotlib.image as mpimg
#from cStringIO import StringIO
#import graphviz as gv
#import externalLibraries.svgtoipe as svgtoipe



#TODO:
#====
# Python 3 and newer versions of NetworkX
# edge Labels can be KirchExpr themselves
# choose between which enumeration algorithm to use -- recursive or not update functions!
# add positivity assumptions for functions in sympify
# fast enumeration without tree structure (as an indet)
# big determinants of large integer matrices
# visualize coarse-grained expressions
# write a general method to traverse expression trees -- minimize code repetition for traversal
# functions to add vertices and edges, combine digraphs, ... and other useful digraph operations
# NOTE: use 64-bit python -- larger part of memory available 

class KirchExpr(object):
    """KirchExpr -- base class for expressions of Kirchhoff polynomials.

    KirchExpr is inherited by the classes:
        - ZERO:     zero,
        - UNIT:     one,
        - CONST:    constant,
        - INDET:    symbol/variable (indeterminate), usually representing a digraph edge label,
        - ADD:      addition operation,
        - NEG:      negation operation (acts as substraction),
        - MUL:      multiplication operation,
        - FRAC:     division operation,
        - POWER:    exponentiation operation (fractional powers possible, e.g. 1/2 used for square root),
        - DERIV:    differentiation,
        - INTEGRAL: integration,
        - KirchMDigraph: kirchhoff polynomial of a digraph, \kappa(G)

    Parameters
    ---------
    children:       an expression tree composed of objects from the inherited classes 
    name:           name of the instance, e.g. what the expression represents
    exprAttributes: dictionary of attributes related to the expression

    """
    # === static variables ===
    tokenCounter = 0
    # ========================
    
    def __init__(self, children=None, name=None, exprAttributes=None):
        if name is not None:
            self.name = name
        else:
            self.name = 'expr'
        
        self.exprAttributes = exprAttributes
        self.children = []

        if children is not None:
            for child in children:
                if not isinstance(child, KirchExpr): raise TypeError('All children should be KirchExpr!')
                self.addChild(child)

    
    def addChild(self, child):
        """addChild -- adds *child* to the children of the KirchExpr instance.
        """
        if not isinstance(child, KirchExpr): raise TypeError('Added child is not a KirchExpr!')
        self.children.append(child)


    def removeChildren(self):
        """removeChildren -- removes all children from the KirchExpr instance.
        """
        self.children = []


    def getChildren(self):
        """getChildren -- retrieve a list of deep copies of the children of self.
        """
        return [child.copy() for child in self.children]


    def copy(self):
        """copy - return a deep copy of the KirchExpr.
        """
        return deepcopy(self)


    @staticmethod
    def nextTokenId():
        """nextTokenId -- generate a unique tokenid.
        """
        tknid = cmn.tokenName + str(KirchExpr.tokenCounter)
        KirchExpr.tokenCounter += 1
        return tknid


    def checkIfAtomic(self):
        """checkIfAtomic -- checks if self is an instance of an atomic class --  ZERO, UNIT, CONST, or INDET.
        """
        return isinstance(self, ZERO) or isinstance(self, UNIT) or isinstance(self, CONST) or isinstance(self, INDET)                


    def checkIfOperation(self):
        """checkIfOperation -- checks if self is an instance of an operation class --  ADD, NEG, MUL, FRAC, POWER, DERIV, or INTEGRAL.
        """
        return isinstance(self, ADD) or isinstance(self, MUL) or isinstance(self, NEG) or isinstance(self, FRAC) or isinstance(self, DERIV) or isinstance(self, POWER) or isinstance(self,INTEGRAL)
    
    
    def checkIfZero(self):
        """checkIfZero -- checks if the expression is zero. =0 -> True; \neq 0 -> False
        """
        if isinstance(self, ZERO):
            return True
        elif self.checkIfAtomic():
            return False
        elif isinstance(self, KirchMDigraph):
            return self.checkArbExistence()
        else:            
            expr = self.getKrichMDigraphsAndSubstWithIndets(checkForDigrsWithNoArbs=True)
            exprSimpl = expr.simplify()
            return isinstance(exprSimpl, ZERO)
        
        
    def getIndeterminates(self, edgeAttribute=None, returnTokenId=False):
        """getIndeterminates -- retrieve the indeterminates/variables/edge labels (from INDET class instances) in KirchExpr corresponding to edgeAttribute.

        Parameters
        ---------
        edgeAttribute: edge attribute name of KirchMDigraph (corresponding to desired edge labels) for which the indeterminates/variables are returned (default edgeAttribute=None)
                       If edgeAttribute=None the default key is uqLabelKey defined in common.py.
        returnTokenId: return the tokenId value of the INDET and not the value (default returnTokenId=False).
       
        NOTE: It could happen that not all indeterminates take part in Kirchhoff polynomials --> some indeterminates might not be relevant (either edge labels not taking part in any arborescnece or user-defined variables).
        """
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)

        if self.checkIfAtomic():
            if isinstance(self, INDET):
                if returnTokenId:
                    return [(self.value, self.tokenId)]
                else:
                    return [self.value]
            else:
                return []
        elif self.checkIfOperation(): # if operation recurse to the leaves
            return list(set(cmn.flatten([c.getIndeterminates(edgeAttribute=edgeAttribute, returnTokenId=returnTokenId) for c in self.children])))
        elif isinstance(self, KirchMDigraph): # if a leaf is a KirchMDigraph, its edge labels are the indeterminates; 
            return [e[2][edgeAttribute] for e in self.getEdges(data=True)]
        else:
            raise NotImplementedError('getIndeterminates: Not implemented for '+ str(type(self)))


    def getKrichMDigraphsAndSubstWithIndets(self, digrs=None, edgeAttribute=None, checkForDigrsWithNoArbs=False):
        """getKrichMDigraphsAndSubstWithIndets -- retrieve KirchMDigarphs from an expression and substitute them with INDETs having unique identifiers.

        Parameters
        ----------
        digrs: an empty list passed by reference to which KirchMDigraphs are appended (default digrs=None)
        edgeAttribute: (default edgeAttribute=None)
        checkForDigrsWithNoArbs: (default checkForDigrsWithNoArbs=False)
        """
        # Go recursively through the expression tree
        if self.checkIfOperation():# not leaves
            newChildren = [child.getKrichMDigraphsAndSubstWithIndets(digrs=digrs, edgeAttribute=edgeAttribute) for child in self.getChildren()]
            self.children = newChildren
            return self
        elif self.checkIfAtomic():# leaves that need not be prime factorised/decomposed
            return self
        elif isinstance(self, KirchMDigraph):# expression tree leaves that have to be factorised/decomposed
            currKirchMDigraph = self.copy()
            if checkForDigrsWithNoArbs:
                exist = currKirchMDigraph.checkArbExistence()
                if not exist:
                    return ZERO()
            # base case -- one or two vertices (and arbs always exist in prime factors/components!)
            if currKirchMDigraph.size() <= 2:
#                if digrs is not None: # append the KirchMDigraph to the digrs list passed by reference
#                    digrs.append(currKirchMDigraph)
                return currKirchMDigraph.labelSum(edgeAttribute)                
            else:
                newTokenId = KirchExpr.nextTokenId()
                currKirchMDigraph.tokenId = newTokenId
                # add base case
                if digrs is not None: # append the KirchMDigraph to the digrs list passed by reference
                    digrs.append(currKirchMDigraph)
                return INDET(cmn.linkVal, currKirchMDigraph.name, newTokenId)
        else:
            raise NotImplementedError('getKrichMDigraphsAndSubstWithIndets: Not implemented for '+ str(type(self)))        


    def generateExpression(self, appearance='default', edgeAttribute=None, enumArbs=False,  heuristic=None, factorIdDict=None, algorithm=None, reportSteps=None, showDigrsWithPossiblyEqualKirchPols=False):
        """generateExpression -- generate a string corresponding to the expression tree of self.

        Parameters
        ---------
        appearance:    format of the string (default is appearance='default'). Formatting options are defined in the dictionary opDict in common.py
        edgeAttribute: edge attribute name of KirchMDigraph (corresponding to desired edge labels) to be considered as variables/indeterminates when generating the expression (default edgeAttribute=None). If edgeAttribute=None the default key is uqLabelKey defined in common.py.
        enumArbs:     generate Kirchhoff polynomials/enumerate the arborescences for the KirchMDigraph objects in the expression (default enumerateArbs=False)
                          True -- enumerate arborescences and generate expressions for KirchMDigraph objects in the expression.
                          False -- consider KirchMDigraph as functions of their edgeAttribute labels.
        heuristic:    a heuristic to use for arborescence enumeration of KirchMDigraph objects in case enumerateArbs=True (default heuristic=None). The heuristics are defined in the method getEdgeForHeuristicDeletionContraction of the KirchMDigraph class and are a sequence of four subheuristics, e.g. (1,1,1,1)
        factorIdDict: dictionary containing unique identifiers for each KirchMDiGraph in the expression (tokenId) and the corresponding function name shared with all other KirchMDigraphs with the same Kirchhoff polynomial
        algorithm:    algorithms to be used for Kirchhoff polynomial generation (default algorithm='implicit_full').
                           Algorithms:
                               - 'recursive' -- C_R
                               - 'implicit_full'-- C_I with substitutions applied (outputs a single Kirchhoff polynomials)
                               - 'implicit' -- C_I without applying the substitutions (outputs a Kirchhff polynomial in CompactKirchPol form)
        reportSteps:  number of iteration steps after which information about the Kirchhoff polynomial generation is reported; works only for C_I (default reportSteps=None).
        showDigrsWithPossiblyEqualKirchPols: output the digarphs for which the necessary condition for Kirchhoff polynomial equality holds but not the sufficient one; they could still have equal Kirchhoff polynomials (default showDigrsWithPossiblyEqualKirchPols=False).
                
        NOTE: When KirchMDigraphs are not prime factorised and they are represented as functions of their labels(variables/indeterminates), it might be that there are niusance edges, i.e. it is possible that not all labels are contained in the Kirchhoff polynomial!
        """
        if not enumArbs and factorIdDict is None: # assign unique tokens to digraphs without factorising and decide which digraphs have the same Kirchhoff polynomial
            factorIdDict = self.getEquivalenceClassesOfKirchMDigraphsWithinKirchExpr(edgeAttribute=edgeAttribute, showDigrsWithPossiblyEqualKirchPols=showDigrsWithPossiblyEqualKirchPols, primeFactorise=False)[0]
            
        inputVector = [appearance, edgeAttribute, enumArbs, heuristic, factorIdDict, algorithm, reportSteps]

        # format of backets w.r.t. the appearance variable 
        openBr  = cmn.opDict[appearance]['open bracket']
        closeBr = cmn.opDict[appearance]['close bracket']
        openF   = cmn.opDict[appearance]['open function']
        closeF  = cmn.opDict[appearance]['close function']

        if isinstance(self, ADD):
            plus = cmn.opDict[appearance]['plus']
            return openBr + plus.join([child.generateExpression(*inputVector)
                                       if not isinstance(child, NEG)
                                       else openBr + child.generateExpression(*inputVector) + closeBr
                                       for child in self.children]) + closeBr
        elif isinstance(self, MUL):
            times = cmn.opDict[appearance]['times']
            return times.join([child.generateExpression(*inputVector) for child in self.children])
        elif isinstance(self, NEG):
            minus = cmn.opDict[appearance]['minus']
            soleChild = self.children[0]
            soleChildGeneratedExpr = soleChild.generateExpression(*inputVector)
            if soleChild.checkIfAtomic() or isinstance(soleChild, ADD): # ADD has brackets
                return openBr + minus + soleChildGeneratedExpr + closeBr
            else: # put brackets
                return openBr + minus + openBr + soleChildGeneratedExpr + closeBr + closeBr
        elif isinstance(self, FRAC):
            div = cmn.opDict[appearance]['division']
            numChild    = self.children[0]
            denomChild  = self.children[1]
            numerator   = numChild.generateExpression(*inputVector)
            denominator = denomChild.generateExpression(*inputVector)
            if numChild.checkIfAtomic() or isinstance(numChild, ADD): 
                numExpr = numerator
            else: # put brackets
                numExpr = openBr + numerator + closeBr
            if denomChild.checkIfAtomic() or isinstance(denomChild, ADD): # ADD has brackets
                denomExpr = denominator
            else: # put brackets
                denomExpr = openBr + denominator + closeBr
            return numExpr + div + denomExpr
        elif isinstance(self, DERIV):
            der = cmn.opDict[appearance]['derivative']
            soleChild = self.children[0]
            diffVars  = [v.value for v in self.edgeVars]
            return der + openF + soleChild.generateExpression(*inputVector) + cmn.comma + cmn.comma.join(diffVars) + closeF            
        elif isinstance(self, INTEGRAL):
            integral = cmn.opDict[appearance]['integral']
            soleChild = self.children[0]
            intVar  = self.edgeVar
            return integral + openF + soleChild.generateExpression(*inputVector) + cmn.comma + intVar.value + closeF
        elif self.checkIfAtomic():
            return self.value
        elif isinstance(self, POWER):
            pwr = cmn.opDict[appearance]['power']
            soleChild = self.children[0]
            exponent = self.exponent.generateExpression(*inputVector)
            # NOTE: no brackets for atomic object since rational numbers are taken care of
            if self.exponent.checkIfAtomic():
                exp = pwr + exponent
            else:
                exp = pwr + openBr + exponent + closeBr
            if soleChild.checkIfAtomic():
                return soleChild.generateExpression(*inputVector) + exp
            else:
                return openBr + soleChild.generateExpression(*inputVector) + closeBr + exp
        elif isinstance(self, KirchMDigraph):
            # TODO: error handilig -- make sure the names are unique after digraph operations are applied!
            if enumArbs: # enumerate arborescences 
                arbs = self.enumerateArbs(edgeAttribute=edgeAttribute, heuristic=heuristic, algorithm=algorithm, reportSteps=reportSteps)
                return arbs.generateExpression(*inputVector) # brackets?
            else:
                # NOTE: Caution! Possible that not all variables/indeterminates take part in the Kirchhoff polynomial!
                edgeLabels = self.getEdgeAttribute(edgeAttribute=edgeAttribute)
                if factorIdDict is not None:
                    funcName = factorIdDict[self.tokenId]
                else:
                    funcName = cmn.functionTag + self.name + self.tokenId
                return  funcName + openF + cmn.comma.join(edgeLabels)  + closeF
        else:
            raise NotImplementedError('generateExpression: Not implemented for '+ str(type(self)))


    def enumArbsInPlace(self, edgeAttribute=None, heuristic=None, algorithm=None, reportSteps=None):
        """enumArbsInPlace -- enumerates the arborescences/generates the Kirchhoff polynomials for every KirchMDigraph in the expression 'in place' (overwriting the non-generated KirchExpr). Additionally to enumerating 'in place', returns an generated KirchExpr.
                              
        Parameters
        ---------
        edgeAttribute:  edge attribute name of KirchMDigraph (corresponding to desired edge labels) to be considered as variables/indeterminates when generating the expression (default edgeAttribute=None).If edgeAttribute=None the default key is uqLabelKey defined in common.py.
        heuristic:      a heuristic to use for arborescence enumeration of KirchMDigraph objects in case enumerateArbs=True (default heuristic=None).The heuristics are defined in the method getEdgeForHeuristicDeletionContraction of the KirchMDigraph class and are a sequence of four subheuristics, e.g. (1,1,1,1).
        algorithm:      algorithms to be used for Kirchhoff polynomial generation (default algorithm='implicit_full').
                           Algorithms:
                               - 'recursive' -- C_R
                               - 'implicit_full'-- C_I with substitutions applied (outputs a single Kirchhoff polynomials)
                               - 'implicit' -- C_I without applying the substitutions (outputs a Kirchhff polynomial in CompactKirchPol form)
        reportSteps:    number of iteration steps after which information about the Kirchhoff polynomial generation is reported; works only for C_I (default reportSteps=None).
        """
        assert algorithm!='implicit', 'enumArbsInPlace: Implicit form not supported yet.'
        
        # Go recursively through the expression tree and enumerate/generate KirchMDigraph's arborescences/Kirchhoff polynomials 'in place'
        if self.checkIfOperation():# not leaves
            enumeratedChildren = [child.enumArbsInPlace(edgeAttribute=edgeAttribute, heuristic=heuristic, algorithm=algorithm, reportSteps=reportSteps) for child in self.getChildren()]
            self.children = enumeratedChildren
            return self
        elif self.checkIfAtomic():# leaves that need not be prime factorised/decomposed
            return self
        elif isinstance(self, KirchMDigraph):# expression tree leaves that have to be factorised/decomposed
            enumKirchMDigraph = self.enumerateArbs(edgeAttribute=edgeAttribute, heuristic=heuristic, algorithm=algorithm, reportSteps=reportSteps)
            if isinstance(enumKirchMDigraph, MUL):
                self.__class__ = MUL # CLASS CONVERSION APPLIED!
                (self.children, self.name, self.category) = (enumKirchMDigraph.children, enumKirchMDigraph.name, enumKirchMDigraph.category)
            if isinstance(enumKirchMDigraph, ADD):
                self.__class__ = ADD # CLASS CONVERSION APPLIED!
                (self.children, self.name, self.category) = (enumKirchMDigraph.children, enumKirchMDigraph.name, enumKirchMDigraph.category)
            if isinstance(enumKirchMDigraph, INDET):
                self.__class__ = INDET # CLASS CONVERSION APPLIED!
                (self.value, self.name) = (enumKirchMDigraph.value, enumKirchMDigraph.name)
            if isinstance(enumKirchMDigraph, UNIT):
                self.__class__ = UNIT # CLASS CONVERSION APPLIED!
                (self.value, self.name) = (enumKirchMDigraph.value, enumKirchMDigraph.name)
            if isinstance(enumKirchMDigraph, ZERO):
                self.__class__ = ZERO # CLASS CONVERSION APPLIED!
                (self.value, self.name) = (enumKirchMDigraph.value, enumKirchMDigraph.name)                
            return enumKirchMDigraph
        else:
            raise NotImplementedError('enumArbsInPlace: Not implemented for '+ str(type(self)))


# TODO: merge enumArbsInPlace and primeFactoriseInPlace into one method; 
    def primeFactoriseInPlace(self, primeComponents=None, primeFactorise=True):
        """primeFactoriseInPlace -- factorises/decomposes all KirchMDigraph objects in the KirchExpr into prime factors/components 'in place' (overwriting the non-factorised object) and assigns unique identifiers to all prime factorised KirchMDigraphs. Additionally to factorising 'in place', returns a factorised KirchExpr.

        Parameters
        ---------
        primeComponents: an empty list passed by reference to which the prime components/factors are appended (default primeComponents=None)
        primeFactorise:  whether to apply prime factorisation (True or False) (default primeFactorise=True).
                             If prime factorisation is not applied the function changes:
                                 - the tokenId of the digraphs in the expression to new unique ones,
                                 - all digarphs in the expression are considered as prime, since factorisability information is not included.
                             Therefore, the function can be used to retrieve the KirchMDigraphs from the expression and assign them unique tokenIds.
        
        Note: Every invokation with primeComponents not equal None changes the tokenId of the digraphs!        
        """
        # Go recursively through the expression tree and factorise all KirchMDigraphs 'in place'
        if self.checkIfOperation():# not leaves
            factorisedChildren = [child.primeFactoriseInPlace(primeComponents, primeFactorise) for child in self.getChildren()]
            self.children = factorisedChildren 
            return self
        elif self.checkIfAtomic():# leaves that need not be factorised/decomposed
            return self
        elif isinstance(self, KirchMDigraph):# expression tree leaves that have to be factorised/decomposed into prime factors/components
            if primeFactorise:
                factorised = self.primeFactorisation()
                if isinstance(factorised, MUL):
                    # assign unique id for each factor/component
                    for i in range(len(factorised.children)):
                        factorised.children[i].tokenId = KirchExpr.nextTokenId()
                        self.__class__ = MUL # CLASS CONVERSION APPLIED!
                        (self.children, self.name, self.category) = (factorised.children, factorised.name, factorised.category)
                    if primeComponents is not None: # append the prime factors/components to the primeComponents list passed by reference
                        primeComponents.append(factorised.getChildren())
                elif isinstance(factorised, KirchMDigraph):
                    # assign unique id
                    currtokenId = KirchExpr.nextTokenId()
                    factorised.tokenId = currtokenId
                    self.tokenId = currtokenId
                    if primeComponents is not None: # append the prime factors/components to the primeComponents list passed by reference
                        primeComponents.append([factorised])
                return factorised
            else: # only give unique token ids
                # consider all of the KirchMDigraphs as prime since no information on their factorisability is included
                self.tokenId = KirchExpr.nextTokenId()
                if primeComponents is not None: # append the prime factors/components to the primeComponents list passed by reference
                    primeComponents.append([self])
                return self
        else:
            raise NotImplementedError('primeFactoriseInPlace: Not implemented for '+ str(type(self)))


    @staticmethod
    def getDigraphsWithEqualKirchPols(primeComponents, showDigrsWithPossiblyEqualKirchPols=False, edgeAttribute=None):
        """getDigraphsWithEqualKirchPols -- return the indices of digraphs from the list of lists *primeComponents* that have equal Kirchhoff polynomials. It is assumed that edgeAttribute labels are unique.
                Conditions to certify the equality used:
                    - Necessary condition:  k(G) = k(H) => l(G)=l(H)
                    - Sufficient condition: E(L(G)) = E(L(H)) => k(G) = k(H)
                    - Isomorphism (categoricalEdgeLabelMatchIsomorphism): lambda-isomorphism check without constructing line digraphs
                        
            Parameters
            ---------
            primeComponents: a list of lists containing the prime factors of prime factorised KirchMDigraphs which are to be compared.
        showDigrsWithPossiblyEqualKirchPols: output the digarphs for which the necessary condition for Kirchhoff polynomial equality holds but not the sufficient one; they could still have equal Kirchhoff polynomials (default showDigrsWithPossiblyEqualKirchPols=False).
            edgeAttribute: edge attribute name of KirchMDigraph (corresponding to desired edge labels) to be considered as variables/indeterminates when generating the sympy expression (default edgeAttribute=None).  If edgeAttribute=None the default key is uqLabelKey defined in common.py.
        """
        # compare factors/components
        def categoricalEdgeLabelMatchIsomorphism(G1, G2):
            """categoricalEdgeLabelMatchIsomorphism -- check if two digraphs G1 and G2 are lambda-isomorphic using networkx isomorphism.
            """
            em = iso.categorical_multiedge_match(edgeAttribute, '')
            return nx.is_isomorphic(G1.mdigraph, G2.mdigraph, edge_match=em)  # no weights considered

        
        def sufficientCondition(G1, G2):
            """sufficient condition -- compare the sets of labels of of the line digaphs of two prime digraphs to determine lambda-isomorphism
                                       E(L(G)) = E(L(H)) => k(G) = k(H)
            """
            if G1.size() <= 2 and G2.size() <= 2:
                return True # the necessary condition is sufficient when there is a single edge in the simple digraphs G1 and G2
            return G1.getLineDigraphEdges(edgeAttribute)==G2.getLineDigraphEdges(edgeAttribute)


        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)
        graphIdsSameKirchPol = [] # identifiers of digraphs which have equal Kirchhoff polynomials
        graphIdsNotCertifiedEqualityKirchPol = [] # identifiers of digraphs whose Kirchhoff polynomials cannot be certified to be equal (only the necessary condition holds but not the sufficient one)
        
        for i1,i2 in it.combinations(range(len(primeComponents)), 2): # indices for all combinations of factorised KirchMDigraphs
            for j1 in range(len(primeComponents[i1])): # for each prime factor in KirchMDigraph with index j1
                for j2 in range(len(primeComponents[i2])): # and for each prime factor in KirchMDigraph with index j2
                    # compare the two current prime factors from each of the factorisations (2*)
                    currG1 = primeComponents[i1][j1]
                    currG2 = primeComponents[i2][j2]
# TODO: possible optimisation by searchihng first in *graphIdsSameKirchPol*, if a hit is found there is no need to undergo consequent comparisons; would be better in cases when there are many identical factors -- then not all comparisons between all digraphs will be in graphIdsSameKirchPol and the grouping into a sublist of graphs with identicla factors will be done in place
                    # Necessary condition -- compare the sets of labels of two prime digraphs; k(G) = k(H) => l(G)=l(H), G and H prime; if they are not the same, Kirchhoff polynomials cannot be the same
                    if set(currG1.getEdgeAttribute(edgeAttribute=edgeAttribute)) == set(currG2.getEdgeAttribute(edgeAttribute=edgeAttribute)):
                      # sufficient condition -- compare the edges of the line digraphs corresponding to two uniquely labelled prime digraphs; E(L(G)) = E(L(H)) => k(G) = k(H).
                        #sufficientConditionTestFunction = categoricalEdgeLabelMatchIsomorphism
                        sufficientConditionTestFunction = sufficientCondition
                            
                        if sufficientConditionTestFunction(currG1, currG2):
                            graphIdsSameKirchPol.append(((i1,j1),(i2,j2)))
                            break # no other factor can be matched to the current prime factorisation indexed by i2,j2 (3*)
                        else:
                            graphIdsNotCertifiedEqualityKirchPol.append(((i1,j1),(i2,j2)))
                            if showDigrsWithPossiblyEqualKirchPols:
                                #TODO: save these digaphs to a file
                                print '<<< Digraphs whose equality cannot be certified by lambda-isomorphism'
                                print currG1.getEdges(data=True)
                                print currG2.getEdges(data=True)
                                currG1.plot(edgeAttribute=edgeAttribute)
                                currG2.plot(edgeAttribute=edgeAttribute)                           
                                print '>>>'

        def groupPairs(listoflists):
            """groupPairs -- determine groups of digraphs with equal Kirchhoff polynomials by representing identicalFactorsIds as a graph (nodes are coordinates of a prime factor within a factorisation of a KirchMDigraph) and retrieving its connected components
            """
            identicalFactorsIdsGraph = nx.Graph()
            identicalFactorsIdsGraph.add_edges_from(listoflists)
            # NOTE: in older versions of NetworkX (e.g. 1.9.1) connected_components returns a generator of lists and in newer-- a generator of sets
            #return [list(c) if isinstance(c,set) else c for c in nx.connected_components(identicalFactorsIdsGraph)]
            return [list(c) for c in nx.connected_components(identicalFactorsIdsGraph)]

        identicalGroupedIds  = groupPairs(graphIdsSameKirchPol)
        uncertainEqualityIds = groupPairs(graphIdsNotCertifiedEqualityKirchPol)
        if showDigrsWithPossiblyEqualKirchPols:
            #TODO: save the digraphs with uncertainEqualityIds to a file
            pass
        
        return identicalGroupedIds, uncertainEqualityIds


    def getEquivalenceClassesOfKirchMDigraphsWithinKirchExpr(self, edgeAttribute=None, showDigrsWithPossiblyEqualKirchPols=False, primeFactorise=True):
        """getEquivalenceClassesOfKirchMDigraphsWithinKirchExpr -- determine which kirchMDigraphs or their prime factorisations have equal Kirchhoff polynomials. Determine the equivalence classes of digraphs with respect to their corresponding Kirchhoff polynomials.
        
        Parameters
        ---------
        edgeAttribute: edge attribute name of KirchMDigraph (corresponding to desired edge labels) to be considered as variables/indeterminates when generating the sympy expression (default edgeAttribute=None). If edgeAttribute=None the default key is uqLabelKey defined in common.py.
        showDigrsWithPossiblyEqualKirchPols: output the digarphs for which the necessary condition for Kirchhoff polynomial equality holds but not the sufficient one; they could still have equal Kirchhoff polynomials (default showDigrsWithPossiblyEqualKirchPols=False).
        primeComponents: pass the prime components as a list of lists, corresponding to the prime factors of each digraph (default primeComponents=None)
        primeFactorise: prime factorise KirchMDigraphs and compare factors (default primeFactorise=True)
        
        Returns
        -------
        (funcDict, primeComponents) = (dictionary mapping the unique factor identifiers(tokens) to a name shared by factors with the same Kirchhoff polynomial, prime components/factors for each considered KirchMDigraph or just digraphs in the expressions if primeFactorise=False)
        """
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)

       # factorise all KirchMDigraph leaves into prime components 'in place' and in parallel extract all prime KirchMDigraphs
        primeComponents = [] # pass 'by reference' this mutable object (list)
        self.primeFactoriseInPlace(primeComponents=primeComponents, primeFactorise=primeFactorise)
        
        # comparison of digraphs (prime or not prime depending on primeFactorise's value)
        allFactorsIds = cmn.flatten([[(i,j) for j in range(len(primeComponents[i]))] for i in range(len(primeComponents))])

        # TODO: decide manually how to treat the digarphs from uncertainEqualityIds (as equal or not)
        identicalGroupedIds, uncertainEqualityIds = KirchExpr.getDigraphsWithEqualKirchPols(primeComponents, showDigrsWithPossiblyEqualKirchPols, edgeAttribute)
        
        # digraph factors whose Kirchhoff polynomial is unique among the compared factors
        uniqueFactors = list(set(allFactorsIds) - set(cmn.flatten(identicalGroupedIds)))
        
        # assign function names to the different digraph factors (identified with tokenId attribute of KirchMDigraph)
        funcDict = {} # contains unique_digraph_id:name_shared_with_factors_with_identical_Kirchhoff_polynomials
        functionCounter = 0
        
        for curr in uniqueFactors:
            currTokenId = primeComponents[curr[0]][curr[1]].tokenId
            funcDict[currTokenId] = cmn.functionTag + str(functionCounter)
            functionCounter += 1

        for curr in identicalGroupedIds:
            ff = cmn.functionTag + str(functionCounter)
            for currsame in curr:
                currTokenId = primeComponents[currsame[0]][currsame[1]].tokenId
                funcDict[currTokenId] = ff
            functionCounter += 1

        return (funcDict, primeComponents)


    def KirchExprToSymPy(self, edgeAttribute=None, showDigrsWithPossiblyEqualKirchPols=False):
        """KirchExprToSymPy -- convert a KirchExpr to a SymPy expression for symbolic manipulation/simplification.
        
        Parameters
        ---------
        edgeAttribute: edge attribute name of KirchMDigraph (corresponding to desired edge labels) to be considered as variables/indeterminates when generating the sympy expression (default edgeAttribute=None). If edgeAttribute=None the default key is uqLabelKey defined in common.py.
        showDigrsWithPossiblyEqualKirchPols: output the digarphs for which the necessary condition for Kirchhoff polynomial equality holds but not the sufficient one; they could still have equal Kirchhoff polynomials (default showDigrsWithPossiblyEqualKirchPols=False).
        
        Returns
        -------
        (sympyExpr,funcDict, primeComponents) = (sympy expression, dictionary mapping the unique factor identifiers(tokens) to a name shared by factors with equal Kirchhoff polynomials, prime components/factors for each considered KirchMDigraph)
        """
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)

        (funcDict, primeComponents) = self.getEquivalenceClassesOfKirchMDigraphsWithinKirchExpr(edgeAttribute=edgeAttribute, showDigrsWithPossiblyEqualKirchPols=showDigrsWithPossiblyEqualKirchPols)

        # NOTE: appearance = 'default' should be compatible with SymPy!
        sympyExprString = self.generateExpression(factorIdDict=funcDict, edgeAttribute=edgeAttribute)

        # ADD LOCAL ASSUMPTIONS that all variables are positive! Needed for simplification of square roots!
        syms = sympy.S(sympyExprString).atoms(sympy.Symbol)
        symdict = dict((i.name, sympy.Symbol(i.name, positive=True)) for i in syms)
                
#        # TODO: SymPy 1.0.0 does not provide local assumptions for functions!?
#        # add also local assumtions for the functions (representing Kirchhoff polynomials); assume positivity.
#        functions = sympy.S(sympyExprString).atoms(sympy.Function)
#        symdict.update(dict((str(i.func), sympy.Function(str(i.func), positive=True)) for i in functions) )
        
        sympyExpr = sympy.sympify(sympyExprString, locals=symdict)
        return (sympyExpr, funcDict, primeComponents)


    @staticmethod
    def SymPyToKirchExpr(sympyExpr, funcDict, primeComponents, edgeAttribute=None):
        """SymPyToKirchExpr -- static method to convert a KirchExpr to a SymPy expression.

        Parameters
        ---------
        sympyExpr: SymPy expression corresponding to the expression of Kirchhoff polynomials that is analysed
        funcDict: dictionary containing unique identifiers for each KirchMDiGraph in the expression (tokenId) and the corresponding function name shared with all other KirchMDigraphs with equal Kirchhoff polynomials
        primeComponents: list of lists containing the prime factors/components of prime factorised/decomposed KirchMDigraphs which are to be compared
        edgeAttribute: edge attribute name of KirchMDigraph (corresponding to desired edge labels) considered as variables/indeterminates when the sympy expression was generated (default edgeAttribute=None). If edgeAttribute=None the default key is uqLabelKey defined in common.py.
        
        """
        def getKirchMDigraphWithFunctionName(fcName):
            """getKirchMDigraphWithFunctionName -- retrive a digarph with a Kirchhoff polynomial corresponding to the function *fcName*.
            
            Parameters
            ---------
            fcName: function name for which a corresponding (with equal Kirchhoff polynomial) KirchMDigraph is to be retrieved
            """
            # NOTE: several digraphs (with different tokens) might correspond to the same function, but they have equal Kirchhoff polynomials
            tokenId = cmn.invertDict(funcDict)[fcName][0]
            grWithTokenId = [g for g in cmn.flatten(primeComponents) if g.tokenId == tokenId]
            if len(grWithTokenId) == 1:
                return grWithTokenId[0].copy()
            else:
                raise LookupError('getKirchMDigraphWithFunctionName: grWithTokenId did not find a (unique) digraph corresponding to the function name ' + fcName)
            
        # === body ===
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)
        inputVector = [funcDict, primeComponents, edgeAttribute]            

        if sympyExpr.is_Add:
            return ADD([KirchExpr.SymPyToKirchExpr(arg,*inputVector) for arg in sympyExpr.args])
            
        elif sympyExpr.is_Mul:
            # TODO: check if there is an odd number of -1 as a multiplier
            # TODO: do not include 1 in the multiplication
            # check whether the current multiplication has a term to the power of -1 => convert to fraction (FRAC)
            numerator   = []
            denominator = []
            for arg in sympyExpr.args:
                if isinstance(arg, sympy.Pow):
                    if arg.exp.is_negative: # negative power -> in the denominator
                        denominator.append(KirchExpr.SymPyToKirchExpr(sympy.Pow(arg.base,sympy.Abs(arg.exp)), *inputVector))
                    else: # positive power -> in the numerator
                        numerator.append(KirchExpr.SymPyToKirchExpr(arg, *inputVector))
                else: # not power --> in the numerator
                    numerator.append(KirchExpr.SymPyToKirchExpr(arg, *inputVector))
            # Convert numerator to KirchExpr            
            if len(numerator) > 1:
                numeratorMUL = MUL(numerator)
            elif len(numerator) == 1: # only one element in the list
                numeratorMUL = numerator[0]
            else: # empty list (should never happen)
                raise RuntimeError('SymPyToKirchExpr: There should always be a numerator!')
            # Convert denominator to KirchExpr                
            if len(denominator) > 1:
                denominatorMUL = MUL(denominator)
            elif len(denominator) == 1: # only one element in the list
                denominatorMUL = denominator[0]            
            # check if fraction
            if len(denominator) > 0: # fraction
                return FRAC(numeratorMUL, denominatorMUL)
            else: # no denominator --> not fraction
                return numeratorMUL

        elif sympyExpr.is_Pow:
            if sympyExpr.exp.is_negative: # negative power -> in the denominator --> fraction
                pwr = KirchExpr.SymPyToKirchExpr(sympy.Pow(sympyExpr.base,sympy.Abs(sympyExpr.exp)), *inputVector)
                return FRAC(UNIT(), pwr)
            elif sympyExpr.exp == 1:
                return KirchExpr.SymPyToKirchExpr(sympyExpr.base, *inputVector)
            else:
                return POWER(KirchExpr.SymPyToKirchExpr(sympyExpr.base, *inputVector), KirchExpr.SymPyToKirchExpr(sympyExpr.exp, *inputVector)) # the exponent 

        elif sympyExpr.is_Function: # KirchMDigraph
            fcName = str(sympyExpr.func)
            return getKirchMDigraphWithFunctionName(fcName)

        elif sympyExpr.is_Symbol: # INDET -- variable
            return INDET(str(sympyExpr))

        elif sympyExpr.is_Number: # all numbers are constants
            if sympyExpr.is_negative:
                absValue = str(sympy.Abs(sympyExpr))
                return NEG(CONST(absValue))
            else:
                if sympyExpr.is_zero:
                    return ZERO()
                elif sympyExpr is sympy.S.One:
                    return UNIT()
                elif sympyExpr.is_Rational: # could be a rational number
                    (num, denom) = sympyExpr.as_numer_denom()
                    if denom is sympy.S.One: # just an integer
                        return CONST(str(sympyExpr))
                    else: # rational number 
                        return FRAC(KirchExpr.SymPyToKirchExpr(num, *inputVector), KirchExpr.SymPyToKirchExpr(denom, *inputVector))
                else:
                    return CONST(str(sympyExpr))

        elif sympyExpr.is_Derivative: # Digraph operations need to be applied if child is KirchMDigraph (edge contraction)
            diffExpr = sympyExpr.args[0]
            diffVars = sympyExpr.args[1:]
            if diffExpr.is_Function: # apply edge contraction!
                fcName = str(diffExpr.func)
                gr = getKirchMDigraphWithFunctionName(fcName)
                for edgeLabel in diffVars:
                    # SymPy 1.4; function "args" returns Tuple for s for the the diff (Derivative when not computed) arguments variables
                    if isinstance(edgeLabel, sympy.Tuple):
                        edgeLabel = edgeLabel[0]
                    
                    # find the edge with the label edgeAttribute:edgeLabel
                    edge = [(e[0],e[1]) for e in gr.getEdgeAttribute(edgeAttribute, onlyAttribute=False) if e[2]==str(edgeLabel)]
                    if len(edge) == 1:
                        gr.contractEdges(edge[0])
                    else:
                        raise ValueError('SymPyToKirchExpr: Cannot differentiate with respect to ' + str(edgeLabel) + ' because labels are not unique or the label is missing!')
                if gr.checkArbExistence():
                    if gr.size()==1:
                        return ZERO()
                    else:
                        return gr
                else:
                    return ZERO()
            else:
                return DERIV(KirchExpr.SymPyToKirchExpr(diffExpr, *inputVector), [INDET[str(e)] for e in diffVars])

        elif isinstance(sympyExpr, sympy.Integral): # Digraph operations need to be applied if child is KirchMDigraph (edge contraction)
            intExpr = sympyExpr.args[0]
            intVar = sympyExpr.args[1][0] # NOTE: integrion wrt one variable is only supported

            if intExpr.is_Function: # apply relabeling l(e)<-l(e)/2, multiply by l(e) and add a constant C
                fcName = str(intExpr.func)
                gr = getKirchMDigraphWithFunctionName(fcName)

                # find the edge with the label edgeAttribute:intVar
                edge = [(e[0],e[1]) for e in gr.getEdgeAttribute(edgeAttribute, onlyAttribute=False) if e[2]==str(intVar)]
                if len(edge) == 1:
                    # NOTE: it is assumed that intVar is a variable; the format of the division sign is hardcoded.
                    newLabel = str(intVar)+'/2'
                    gr.relabelEdge(edge[0], newLabel, edgeAttribute)
                else:
                    raise ValueError('SymPyToKirchExpr: Cannot differentiate with respect to ' + str(edgeLabel) + ' because labels are not unique or the label is missing!')
                if gr.checkArbExistence():
                    newId = KirchExpr.nextTokenId()
                    intConstName = cmn.integrationConstant + newId
                    if gr.size()==1: #1
                        return ADD([INDET(str(intVar)),CONST(intConstName)])
                    else:
                        return ADD([MUL([INDET(str(intVar)),gr]),CONST(intConstName)])
                else: #0
                    return CONST(intConstName)
            else:
                return INTEGRAL(KirchExpr.SymPyToKirchExpr(intExpr, *inputVector), INDET[str(intVar)] )

        else:
            raise NotImplementedError('SymPyToKirchExpr: Not implemented for '+ str(type(sympyExpr)) +' '+ str(sympyExpr))


    def simplify(self, edgeAttribute=None, showDigrsWithPossiblyEqualKirchPols=False, skipSymbolicSimplification=False):
        """simplify -- simplify the expression of Kirchhoff polynomials *self* using SymPy.

        Parameters
        ---------
        edgeAttribute: edge attribute to be used during the simplification when digraphs are considered as functions of edgeAttribute values (default edgeAttribute=None)
        showDigrsWithPossiblyEqualKirchPols: output the digarphs for which the necessary condition for Kirchhoff polynomial equality holds but not the sufficient one; they could still have equal Kirchhoff polynomials (default showDigrsWithPossiblyEqualKirchPols=False).
        skipSymbolicSimplification: do not invoke the function sympy.simplify() but convert it to sympy type; the conversion already performs some simplifications
        
        NOTE: Two simplifications may be needed if the expression contains differentiation due to the application of edge contractions which could uncover new prime factors/components!
        """
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)
        (sympyExpr, funcDict, primeComponents) = self.KirchExprToSymPy(edgeAttribute=edgeAttribute, showDigrsWithPossiblyEqualKirchPols=showDigrsWithPossiblyEqualKirchPols)

        if not skipSymbolicSimplification:
            sympyExpr = sympyExpr.simplify()
            
        simplifiedKirchExpr = KirchExpr.SymPyToKirchExpr(sympyExpr, funcDict, primeComponents, edgeAttribute)

        return  simplifiedKirchExpr

    
    def isEqual(self, expr, edgeAttribute=None, heuristic=None, generateKirchhoffPols=False, showDigrsWithPossiblyEqualKirchPols=False):
        """isEqual -- query checking if *self* is equal to *expr* using SymPy.

        Parameters
        ---------
        expr: KirchExpr to be compared to *self*
        edgeAttribute: edge attribute whose values to be considered during the equality test when digraphs are considered as functions of edgeAttribute values (default edgeAttribute=None)
        heuristic:    a heuristic to use for arborescence enumeration of KirchMDigraph objects in case enumerateArbs=True (default heuristic=None). The heuristics are defined in the method getEdgeForHeuristicDeletionContraction of the KirchMDigraph class and are a sequence of four subheuristics, e.g. (1,1,1,1)
        generateKirchhoffPols: generate the full expression by generating the contained Kirchhoff polynomials, thus eliminating all KirchMDigraps; only then the comparison is made.
        showDigrsWithPossiblyEqualKirchPols: output the digarphs for which the necessary condition for Kirchhoff polynomial equality holds but not the sufficient one; they could still have equal Kirchhoff polynomials (default showDigrsWithPossiblyEqualKirchPols=False).
                
        NOTE: It could happen that equivalent expressions are not in a form which is comparable without full Kirchhoff polynomial generation. For example, when edge deletion-contraction is applied to a digraph, the resulting digarphs, when compared as functions of their labels to the factors of the original digraph, would be different. Only after Kirchhoff polynomial generation, the expressions would be equivalent.

        """
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)

        #NOTE: if self == expr (or A==B), then eqExpr=0
        A = self.copy()
        B = expr.copy()

        if generateKirchhoffPols:
            A.enumArbsInPlace(edgeAttribute=edgeAttribute, heuristic=heuristic)
            B.enumArbsInPlace(edgeAttribute=edgeAttribute, heuristic=heuristic)
        
        eqExpr = ADD([A,NEG(B)])
        (sympyExpr, funcDict, primeComponents) = eqExpr.KirchExprToSymPy(edgeAttribute=edgeAttribute, showDigrsWithPossiblyEqualKirchPols=showDigrsWithPossiblyEqualKirchPols)
        sympyExpr = sympyExpr.simplify()
        # no need to convert it back to KirchExpr (in case sympyExpr!=0 is a long expression)
        return sympyExpr == 0


    def writeKirchExprToFile(self, File, separator=None, edgeAttribute=None, showDigrsWithPossiblyEqualKirchPols=False, drawComponents=False):
        """writeKirchExprToFile -- write (prime factorised) KirchExpr expression to a file.

        Parameters
        ---------
        File: name of the output file; the file extension need not be included, it is defined as *fileExtension* in common.py. The format of the file is defined in initializeKirchExprFromFile. The data entry tags are defined in the *tags* dictionary defined in common.py.
        separator: separator between different data/attributes used in File; the separator should not be part of any key value entries. If separator=None the default separator *defaultSeparator* defined in common.py is used.
        edgeAttribute: edge attribute whose values are used to generate a SymPy expression (default edgeAttribute=None)
        showDigrsWithPossiblyEqualKirchPols: output the digarphs for which the necessary condition for Kirchhoff polynomial equality holds but not the sufficient one; they could still have equal Kirchhoff polynomials (default showDigrsWithPossiblyEqualKirchPols=False).
        drawComponents: visualize digraphs taking part in the expression with pyplot (default drawComponents=False)
        """
        def genLine(tag, dat=[], endWithNewLine=True):
            """genLine -- generate a *separator* separated line for the output file.
            """       
            if not isinstance(dat, list):
                dat = [dat]
            return  separator.join([cmn.tags[tag]] + dat + [nl])        

        
        if separator is None:
            separator = cmn.defaultSeparator
        
        # short names
        nl   = cmn.newLine 
        cmnt = cmn.comment
 
        header    = cmnt + 'kpy file' + nl
        finalFile = header
        
        if not isinstance(self,KirchMDigraph): # generate an expression when *self* is not a *KirchMDigraph*
            (sympyExpr, funcDict, components) = self.KirchExprToSymPy(edgeAttribute=edgeAttribute, showDigrsWithPossiblyEqualKirchPols=showDigrsWithPossiblyEqualKirchPols)

            # Write the KirchPy expression as a SymPy expression
            finalFile += genLine('expressionNameTag', self.name)
            finalFile += genLine('expressionTag', str(sympyExpr))
    
            if self.exprAttributes is not None:
                finalFile += genLine('expressionAttributesTag', cmn.flattenDict(self.exprAttributes))
            finalFile += genLine('endTag') + nl
        else: # when self is KirchMDigraph only write the digraph to the file
            self.tokenId = '0'
            components = [self.copy()]
            funcDict = {'0':'K0'}
            
        # Write the KirchMDigraphs (represented as functions) from the KirchPy expression
        functionsWritten = set()
        
        for gr in cmn.flatten(components):
            currFuncName = funcDict[gr.tokenId]
            if currFuncName not in functionsWritten:# do not write KirchMDigraphs with identical function name (equal Kirchhoff polynomials) although having different tokenId
                functionsWritten.add(currFuncName)
                
                # visualize digraphs with pyplot
                if drawComponents:
                    #TODO: visualise the same prime digraphs in an expression with the same colour
                    primeFact = gr.primeFactorisation() # colour prime components
                    if not isinstance(primeFact,KirchMDigraph):
                        digrs = []
                        primeFact.getKrichMDigraphsAndSubstWithIndets(digrs)
                        decompLabels =  {frozenset(d.getIndeterminates()):i for i,d in enumerate(digrs)}
                    else:
                        decompLabels=None
                    gr.plotPyPlot(File+currFuncName, edgeAttribute=edgeAttribute,decompLabels=decompLabels)
                
                fileString  = genLine('nameTag', gr.name)
                fileString += genLine('arbsTag', gr.arbsType)
                fileString += genLine('tokenIdTag', currFuncName)

                if gr.metadata is not None:
                    fileString += genLine('metaTag', gr.metadata)
                    
                if gr.attributes:
                    fileString += genLine('attributesTag', cmn.flattenDict(gr.attributes))

                if gr.arbsType == 'in': # reverse the digraph if KirchMDigraph was written wrt in-arborescences
                    Gr = gr.mdigraph.reverse(copy=True)
                else:
                    Gr = gr.mdigraph
        
                edges = Gr.edges(data=True)
                
                if len(edges) > 0:
                    for e in edges:
                        fileString += genLine('edgeTag', [e[0],e[1]] + cmn.flattenDict(e[2]))
                vertices = Gr.nodes(data=True)
                
                if len(vertices) > 0:
                    for v in vertices:
                        fileString += genLine('vertexTag', [v[0]] + cmn.flattenDict(v[1]))
                        
                        
                def genDatLine(k,v):
                    return cmnt + k + separator + v + nl
                
                # TODO: make more performance information accessible by this function
                dat = [['|V|:', str(gr.size())],
                       ['|E|:', str(gr.getNumberOfEdges())],
                       ['|SCC|:', str(gr.getNumberOfSCCs())], 
                       ['|Arbs|:', str(gr.countArbs())],
                       ['Expanded length:', str(gr.expandedLengthKirchPol())],
                       #['Compressed length:', 'X'],
                       #['Compression ratio:', 'X'],
                       #['Heuristic used:', 'X'],
                       #['Running time:', 'X']
                       ['Kirchhoff polynomial:', str(gr.KirchhoffPolynomial)]]
                
                fileString += ''.join([genDatLine(k,v) for k,v in dat])
                fileString += genLine('endTag') + nl
                finalFile  += fileString
            
        with open(File + cmn.fileExtension, 'w') as f:
            f.write(finalFile)


    def initializeKirchExprFromFile(self, File, edgeAttribute=None, name=None, separator=None):
        """initializeKirchExprFromFile -- read and initilize KirchPy expressions (KirchExpr) from file.
        
        Parameters
        ---------
        File: file name together with its access path 
            Format of the file, for example:
            example.kpy
            ==========
            EXPRNAME \t Expr1
            EXPR \t sympy expression, digraphs are written as functions of their unique labels e.g. f0(Gr1, Gr8)*f1(Gr0)*f2(Gr3, Gr4, Gr9)*f3(Gr7)
            EXPRATTRIBUTES \t key1 \t value1 \t key2 \t value2 \t ...
            END \t
            
            NAME \t Gr1
            TOKENID \t
            META \t informtaion about the digraph
            ARBS \t in (could only be in or out)
            ATTRIBUTES \t key1 \t value1 \t key2 \t value2 \t ...
            EDGE \t v1 \t v2 \t key1 \t value1 \t key2 \t value2 \t ...
            ...
            VERTEX \t v1 \t key1 \t value1 \t key2 \t value2 \t ...
            VERTEX \t v2 \t key1 \t value1 \t key2 \t value2 \t ...
            ...
            END
            
            ...
            NOTE: white spaces are included only for readability
            
            Additionaly: - '#' denotes commented out lines which are not read
                         - several digraphs can be defined in sequence
                         - An entry for a digarph/expression must start with a NAME tag and end with an END tag to enclose the data for the corresponding entry; the order of tags does not matter
                         - required tags for every file are NAME, EDGES, and END
                         - key-value pairs are not mandatory
         
        edgeAttribute: edge attribute whose values are used in the expression (default edgeAttribute=None)
        name: in case there are multiple expressions defined in the same file, *name* denotes which one to read (default= first expression). If there are no expressions defined, then *name* is interepreted as the name of the digraph to be initialized. Again, default is the first digraph if *name* is not user-specified.
        separator: separator between different data/attributes used in File; the separator should not be part of any key value entries. If separator=None the default separator (defaultSeparator) defined in common.py is used.
               
        NOTE: If this method is invoked after initialisation it will overwrite all previously initialised data; the words used for tags are not allowed as vertex names
        NOTE: Tags are defined in *tags* dictionary in common.py
        NOTE: value entries can be dictionaries themselves (python dictionary notation)
        NOTE: ARBS tags the type of arborescences to be analysed; in for in-arborescences and out for out-arborescences
        NOTE: multiple expressions could be written in the same file, but only a single one will be read
        
        """
        # === Local function definitions ===
        def parseKVList(kvlist):
            """parseKVList -- parse key value list.
            """
            # partition the list
            partitionedKVList = [kvlist[i:i+2] 
                                 for i in range(0, len(kvlist), 2)]
            kvdict = {}            
            for key,value in partitionedKVList:
                # check if the value is a dictionary itself
                if value[0]=='{' and value[-1] == '}':# curly bracket 
                    kvdict[key] = ast.literal_eval(value)
                else:
                    kvdict[key] = value
            return kvdict
            
        def checkKVList(kvlist):
            """checkKVList -- check correctness of the kvlist (key-value entries with wrong format); True - the list is fine, False - either does not exist or odd number of elements.
            """
            if len(kvlist) != 0: # it is non-empty
                if len(kvlist)%2 != 0: # odd number of elements
                    raise ImportError('checkKVList: Key-value pairs ' + str(kvlist) + 'are not an even number!')
                else:
                    return True
            else:
                return False

        # === body ===
        if separator is None:
            separator = cmn.defaultSeparator

        # read file as a list
        lines = [line.rstrip() 
                 for line in open(File) 
                 if line[0]!=cmn.comment and line!=cmn.newLine]
        # data in a list of lists 
        dat   = [line.split(separator) for line in lines]

        exprList     = [] # list of dictionaries of the expressions from the file
        digraphList  = [] # list of dictionaries of the digraphs from the file
        edgeList     = [] # edge list
        vertexList   = [] # vertex list
        stateFlag    = 0  # the state flag designates which entity is being read -> 0 -- looking for name tag; 1 -- found expression name tag; 2 -- found digraph name tag;
        currentEntry = {}
        
        for l in dat:
            currTag = l[0] # first element should be the tag

            if currTag == cmn.tags['expressionNameTag']: # expression
                if bool(currentEntry):
                    raise ImportError('initializeKirchExprFromFile: Problem with the sequence of tags!')
                stateFlag = 1
                currentEntry['expressionNameTag'] = l[1]

            elif currTag == cmn.tags['nameTag']: # digarph (KirchMDigraph)
                if bool(currentEntry) or edgeList != [] or vertexList != []:
                    raise ImportError('initializeKirchExprFromFile: Problem with the sequence of tags!')
                stateFlag = 2
                currentEntry['nameTag'] = l[1]

            elif currTag == cmn.tags['endTag']: # end of the data entries for the current expression or digraph
                if stateFlag == 1: # read data for an expression
                    exprList.append(currentEntry.copy())
                elif stateFlag == 2: # read data for a digraph
                    currentEntry['edgeTag']   = edgeList
                    currentEntry['vertexTag'] = vertexList
                    digraphList.append(currentEntry.copy())
                    edgeList   = []
                    vertexList = []
                # reset state --> look for new expressions/digraphs
                stateFlag = 0
                currentEntry = {}
                
            elif stateFlag == 1: # found an expression
                if currTag == cmn.tags['expressionTag']:
                    currentEntry['expressionTag'] = l[1]
                elif currTag == cmn.tags['expressionAttributesTag']:
                    kvlist = l[1:]
                    if checkKVList(kvlist):
                        currentEntry['expressionAttributesTag'] = parseKVList(kvlist)
                                    
            elif stateFlag == 2: # found a digraph
                if currTag == cmn.tags['tokenIdTag']:
                    currentEntry['tokenIdTag'] = l[1]
                elif currTag == cmn.tags['metaTag']:
                    currentEntry['metaTag'] = l[1]
                elif currTag == cmn.tags['arbsTag']:
                    assert l[1] in ['in', 'out'], "initializeKirchExprFromFile: Neither \'in\' nor \'out\' arborescences!" # two options only, in- and out- arborescences
                    currentEntry['arbsTag'] = l[1] # no default value is given since instantiation has already initialized the variable with out!                    
                elif currTag == cmn.tags['attributesTag']:
                    kvlist = l[1:]
                    if checkKVList(kvlist):
                        currentEntry['attributesTag'] = parseKVList(kvlist)
                elif currTag == cmn.tags['edgeTag']:
                    kvlist = l[3:]
                    if checkKVList(kvlist):
                        edgeList.append((l[1],l[2], parseKVList(kvlist)))                    
                    else: # there are no attributes
                        edgeList.append((l[1],l[2]))                    
                elif currTag == cmn.tags['vertexTag']:
                    kvlist = l[2:]
                    if checkKVList(kvlist):
                        vertexList.append((l[1], parseKVList(kvlist)))                    
                    else: # there are no attributes
                        vertexList.append((l[1]))   

        # generate KirchMDigraph for every element of digraphList and funcDict
        funcDict = {}
        mdgrList = []

        # initialize KirchMDigarphs        
        for grDat in digraphList:
            # TODO: perform checks if the input is in the right form
            currEdges =    grDat['edgeTag']       if 'edgeTag'       in grDat else None
            currVertices = grDat['vertexTag']     if 'vertexTag'     in grDat else None
            currMeta =     grDat['metaTag']       if 'metaTag'       in grDat else None
            currArbs =     grDat['arbsTag']       if 'arbsTag'       in grDat else 'out'
            currTokenId =  grDat['tokenIdTag']    if 'tokenIdTag'    in grDat else None
            currAttrbs  =  grDat['attributesTag'] if 'attributesTag' in grDat else None

            currG = KirchMDigraph(name=grDat['nameTag'],
                                  edgedata=currEdges,
                                  vertexdata=currVertices,
                                  metadata=currMeta,
                                  arbsType=currArbs,
                                  tokenId=currTokenId)
            currG.attributes = currAttrbs
            mdgrList.append(currG.copy())          
            funcDict[currTokenId]=currTokenId # here, tokenID corresponds to function name

        # Check if expression definitions are present in the file. If not consider initialize only the first digraph.
        if len(exprList) != 0:
            # initialize the expression
            flagFoundExprName = 0
            # default is the first expression 
            if name is None:
                name = exprList[0]['expressionNameTag']
            # TODO: expression attributes not included!
            #exprAtributes = {}
            for currExpr in exprList:
                if currExpr['expressionNameTag'] == name:
                    flagFoundExprName = 1
                    sympyExpr = sympy.S(currExpr['expressionTag'])
                    #exprAtributes = currExpr['expressionAttributesTag']
                    expr = KirchExpr.SymPyToKirchExpr(sympyExpr, funcDict, list([mdgrList]), edgeAttribute=edgeAttribute)
                    
                    if expr.checkIfOperation():
                        if isinstance(expr, ADD):
                            self.__class__ = ADD # CLASS CONVERSION!
                            self.category = expr.category
                        elif isinstance(expr, MUL):
                            self.__class__ = MUL # CLASS CONVERSION!
                            self.category = expr.category
                        elif isinstance(expr, NEG):
                            self.__class__ = NEG # CLASS CONVERSION!
                        elif isinstance(expr, FRAC):
                            self.__class__ = FRAC # CLASS CONVERSION!
                        elif isinstance(expr, DERIV):
                            self.__class__ = DERIV # CLASS CONVERSION!
                            self.edgeVars = expr.edgeVars
                        elif isinstance(expr, INTEGRAL):
                            self.__class__ = INTEGRAL # CLASS CONVERSION!
                            self.edgeVar = expr.edgeVar
                        elif isinstance(expr, POWER):
                            self.__class__ = POWER # CLASS CONVERSION!
                            self.exponent = expr.exponent
    
                        (self.children, self.name) = (expr.children, expr.name)
    
                    elif expr.checkIfAtomic():
                        if isinstance(expr, ZERO):
                            self.__class__ = ZERO # CLASS CONVERSION!
                        elif isinstance(expr, UNIT):
                            self.__class__ = UNIT # CLASS CONVERSION!
                        elif isinstance(expr, CONST):
                            self.__class__ = CONST # CLASS CONVERSION!
                        elif isinstance(expr, INDET):
                            self.__class__ = INDET # CLASS CONVERSION!
    
                        (self.value, self.name) = (expr.value, expr.name)
    
                    elif isinstance(expr, KirchMDigraph):
                        self.__class__ = KirchMDigraph # CLASS CONVERSION!
                        (self.name, self.metadata, self.mdigraph, self.tokenId, self.attributes, self.arbsType, self.KirchhoffPolynomial, self.numberOfArborescences ) = (expr.name, expr.metadata, expr.mdigraph, expr.tokenId, expr.attributes, expr.arbsType, expr.KirchhoffPolynomial, expr.numberOfArborescences )
                        
                    else:
                        raise NotImplementedError('initializeKirchExprFromFile: Not implemented for '+ str(type(expr)))
                    break
    
            if flagFoundExprName == 0:
                raise ImportError('initializeKirchExprFromFile: The name tag or the provided expression name was not found in ' + File + cmn.fileExtension)

        else: # expressions not defined --> initialize digraph
            flagFoundDigrName = 0
            # name is interpreted as the name of the digraph if expression names are not present; default is the first digraph;
            if name is None:
                name = mdgrList[0].name
    
            for currDigr in mdgrList:
                if currDigr.name == name:
                    flagFoundDigrName = 1
                    self.__class__ = KirchMDigraph # CLASS CONVERSION!
                    (self.name, self.metadata, self.mdigraph, self.tokenId, self.attributes, self.arbsType, self.KirchhoffPolynomial, self.numberOfArborescences ) = (currDigr.name, currDigr.metadata, currDigr.mdigraph, currDigr.tokenId, currDigr.attributes, currDigr.arbsType, currDigr.KirchhoffPolynomial, currDigr.numberOfArborescences )
                    break
    
            if flagFoundDigrName == 0:
                raise ImportError('The name tag or the provided digraph name was not found in ' + File + cmn.fileExtension)


    def exprTreeStructureData(self):
        """exprTreeStructureData -- structure of the expression tree -> number of operations, number of atomic elements, number of KirchMDigraphs and their size (|arb|, |V|, |E|). We define the size of a KirchExpr as the number of vertices in the expression tree.

        """ 
        # Go recursively through the expression tree and count the vertices (including the leaves)
        if self.checkIfOperation():# not leaves
            childData = [child.exprTreeStructureData() for child in self.children]
            kmgData = cmn.flatten([b for a,b in childData])
            return (cmn.addVectors([a for a,b in childData] + [[1, 0, 0]]), kmgData) # ([operation, atomic, KirchMDigraph], kmgData)
        elif self.checkIfAtomic():# leaves that need not be prime factorised/decomposed
            return ([0, 1, 0], []) # ([operation, atomic, KirchMDigraph], kmgData)
        elif isinstance(self, KirchMDigraph):# expression tree leaves that have to be factorised/decomposed
            # (number of arborescences, number of nodes, number of edges)
            kmgData = [(self.countArbs(), self.size(), self.getNumberOfEdges())]
            return ([0, 0, 1], kmgData) # ([operation, atomic, KirchMDigraph], kmgData)
        else:
            raise NotImplementedError('numberVerticesInTheExpressionTree: Not implemented for '+ str(type(self)))


    def getCompressedLength(self):
        """ getCompressedLength -- use only when the Kirchhoff polynomial is fully generated!
        """
        GE = self.exprTreeStructureData()
        if GE[0][2] == 0:
            comprLength = GE[0][0]+GE[0][1]
            return comprLength
        else:
            raise RuntimeError("getCompressedLength: The Kirchhoff polynomials in the expression should be fully generated!")


    def condenseOperationsInExpressionTree(self):
        """condenseOperationsInExpressionTree -- condense MUL([a,MUL([b,...])],...]) to MUL([a,b,...,...]) and ADD([a,ADD([b,...])],...]) to ADD([a,b,...,...]).
        """
        # TODO: condense also for other operations
        # Go recursively through the expression tree
        if self.checkIfOperation():# not leaves            
            if isinstance(self,MUL) or isinstance(self,ADD):
                currType = type(self)
                Q = cmn.Queue()
                for child in self.getChildren():
                    Q.enqueue(child)
#                condChildren = cmn.flatten([child.getChildren() if isinstance(child,currType) else [child] for child in self.getChildren()])
                condChildren = []

                while not Q.isEmpty():
                    currChild = Q.dequeue() # also assumes the initial digraph is not prime
                    if isinstance(currChild,currType):
                        for childchild in currChild.getChildren():
                            Q.enqueue(childchild)                            
                    else:
                        condChildren.append(currChild)

                newChildren = [child.condenseOperationsInExpressionTree() for child in condChildren]
                self.children = newChildren
            return self
        elif self.checkIfAtomic():# leaves that need not be prime factorised/decomposed
            return self
        elif isinstance(self, KirchMDigraph):# expression tree leaves that have to be factorised/decomposed
            return self
        else:
            raise NotImplementedError('condenseOperationsInExpressionTree: Not implemented for '+ str(type(self)))        


    
# ======================================================================================================
# Arithemtic and differential operations, unit and zero of the polynomial ring, indeterminates/variables
# ======================================================================================================
# TODO: implment class SQRT?
# TODO: implement LOG
# === CLASSES FOR ATOMIC ELEMENTS OF EXPRESSIONS OF KIRCHHOFF POLYNOMIALS ===
class ZERO(KirchExpr):
    """ZERO -- class for the zero element of the polynomial ring -- signifies no arborescences present.
               Has default name='zero' and value='0', inherits KirchExpr.
    
    Examples
    --------
    expr = ZERO()
    """ 
    def __init__(self, value='0'):
        KirchExpr.__init__(self, name='zero')
        self.value = value

    def checkArbExistence(self):
        """checkArbExistence -- arborescences do not exist (ZERO is the Kirchhoff polynomial of all digraphs with no arborescences).
        """
        return False


class UNIT(KirchExpr):
    """UNIT -- class for the unit element of the polynomial ring -- trivial arborescence -- single vertex digraph with no edges.
               Has default name='unit' and value='1', inherits KirchExpr.

    Examples
    --------
    expr = UNIT()
    """
    def __init__(self, value='1'):
        KirchExpr.__init__(self, name='unit')
        self.value = value

    def checkArbExistence(self):
        """checkArbExistence -- arborescences exist (UNIT corresponds to the trivial arborescence of a single vertex digraph).
        """
        return True


class CONST(KirchExpr):
    """CONST -- class for a constant value.
                Inherits KirchExpr.
    
    Parameters
    ---------
    value: constant value
    name: name of the instance (default name=None)

    Examples
    --------
    expr = CONST('2.3')
    """
    def __init__(self, value, name=None):
        KirchExpr.__init__(self, name=name)
        self.value = value


class INDET(KirchExpr):
    """INDET -- class for indeterminates/variables -- holds edge labels which can be a rate constant, variable, or any expression.
                Inherits KirchExpr.

    Parameters
    ---------
    value: Parameter/variable name, mathematical expression (in appropriate format)
    name: name of the instance (default name=None)

    Examples
    --------
    expr = INDET('r1')
    expr = INDET('r1^2+1')
    """
    def __init__(self, value, name=None, tokenId=None):
        KirchExpr.__init__(self, name=name)
        self.value = value
        self.tokenId = tokenId


# === CLASSES FOR OPERATIONS ON KIRCHHOFF POLYNOMIALS ===

class ADD(KirchExpr):
    """ADD -- class for the multi-argument addition operator. Inherits KirchExpr.
    
    Parameters:
    -----------
    children: list of KirchExpr summands (must be more than one)
    name: name of the instance (default name=None)
    category: additional information about the summation operation. For example, category='delection-contraction' denotes an expression obtained from deletion contraction of an edge (default category=None).

    Examples
    --------
    expr = ADD([INDET(r1),UNIT(), FRAC(KirchMDigraph('G'),KirchMDigraph('F'))])
    """
    def __init__(self, children, name=None, category=None):
        assert len(children) > 1, 'ADD: At least two expressions need to be added!'
        assert all([isinstance(child, KirchExpr) for child in children]), 'ADD: All children should be KirchExpr objects!'
        KirchExpr.__init__(self, children, name)
        self.category = category


class NEG(KirchExpr):
    """NEG -- class for the negative sign. Inherits KirchExpr.

    Parameters:
    -----------
    child: KirchExpr object
    name: name of the instance (default name=None)

    Examples
    --------
    NEG(CONST(2)) #corresponds to negative -CONST(2)
    ADD(CONST(2),NEG(KirchMDigraph('G'))) #corresponds to CONST(2)+(-KirchMDigraph('G'))=CONST(2)+-KirchMDigraph('G')
    """
    def __init__(self, child, name=None):
        assert isinstance(child, KirchExpr), 'NEG: The child should be a KirchExpr object!'
        KirchExpr.__init__(self, [child], name)


class MUL(KirchExpr):
    """MUL -- class for the multi-argument multiplication operator. Inherits KirchExpr.

    Parameters:
    -----------
    children: list of KirchExpr factors (must be more than one)
    name: name of the instance (default name=None)
    category: additional information about the multiplication operation. For example, category='prime' denotes an expression obtained from prime factorisation and category='delection-contraction' denotes the multiplication of an edge digraph with an edge contracted digraph (default category=None)

    Examples
    --------
    expr = MUL([INDET(r1),UNIT(), FRAC(KirchMDigraph('G'),KirchMDigraph('F'))])
    """
    def __init__(self, children, name=None, category=None):
        assert len(children) > 1, 'MUL: At least two expressions need to be multiplied!'
        assert all([isinstance(child, KirchExpr) for child in children]), 'MUL: All children should be KirchExpr objects!'
        KirchExpr.__init__(self, children, name)
        self.category = category


class FRAC(KirchExpr):
    """FRAC -- class for fraction objects. Inherits KirchExpr.

    Parameters:
    -----------
    numerator: the numerator must be a KirchExpr object
    denominator: the denominator must be a KirchExpr object
    name: name of the instance (default name=None)

    Examples
    --------
    expr = FRAC(KirchMDigraph('G'),ADD([CONST('3'),INDET('r1')]))
    """
    def __init__(self, numerator, denominator, name=None):
        assert isinstance(numerator, KirchExpr),   'FRAC: The numerator should be a KirchExpr object!'
        assert isinstance(denominator, KirchExpr), 'FRAC: The denominator should be a KirchExpr object!'
        KirchExpr.__init__(self, [numerator, denominator], name)
    

class POWER(KirchExpr):
    """POWER -- class for exponentiation (base^exponent). Inherits KirchExpr.

    Parameters:
    -----------
    base: the base must be a KirchExpr object
    exponent: the exponent must be a KirchExpr object
    name: name of the instance (default name=None)

    Examples
    --------
    expr = POWER(KirchMDigraph('G'),CONST('2'))
    expr = POWER(KirchMDigraph('G'),FRAC(UNIT(),CONST('2'))) # square root
    """
    def __init__(self, base, exponent, name=None):
        assert isinstance(base, KirchExpr), 'POWER: The numerator should be a KirchExpr object!'
        assert isinstance(exponent, KirchExpr), 'POWER: The exponent should be a KirchExpr object!'
        KirchExpr.__init__(self, [base], name)
        self.exponent = exponent


class DERIV(KirchExpr):
    """DERIV -- class for taking derivative with respect to unique edge labels (derivative of child with respect to edgeVars). Inherits KirchExpr.

    Parameters:
    -----------
    child: KirchExpr to be differentiated
    edgeVars: list of unique edge labels represented with INDET objects
    name: name of the instance (default name=None)

    Examples
    --------
    expr = DERIV(KirchMDigraph('G'),[INDET('r1')])

    """
    def __init__(self, child, edgeVars, name=None):
        assert isinstance(child, KirchExpr), 'DERIV: The child should be a KirchExpr object!'
        if isinstance(edgeVars, list):
            assert all([isinstance(var, INDET) for var in edgeVars]), 'DERIV: All variables should be INDET objects!'
            self.edgeVars = edgeVars
        elif isinstance(edgeVars, INDET):
            self.edgeVars = [edgeVars]
        else:
            raise TypeError('DERIV: Variables should be INDET objects!')
        KirchExpr.__init__(self, [child], name)


class INTEGRAL(KirchExpr):
    """INTEGRAL -- class for symbolic integration with respect to unique edge labels (integral of child with respect to edgeVars). Inherits KirchExpr.

    Parameters:
    -----------
    child: KirchExpr to be integrated
    edgeVar: unique edge label represented with an INDET object; variable with respect to which the integal is defined
    name: name of the instance (default name=None)

    Examples
    --------
    expr = INTEGRAL(KirchMDigraph('G'),[INDET('r1')])

    """
    def __init__(self, child, edgeVar, name=None):
        assert isinstance(child, KirchExpr), 'INTEGRAL: The child should be a KirchExpr object!'
        assert isinstance(edgeVar, INDET), 'INTEGRAL: Variables should be INDET objects!'
        self.edgeVar = edgeVar # NOTE: allows for integration with respect to a single vairable only!
        KirchExpr.__init__(self, [child], name)

# TODO: implement strong articulation points, domination articulation points, prime bridges, contraction bridges, prime articulation points
class KirchMDigraph(KirchExpr):
    """KirchMDigraph -- class for networkX's MultiDiGraphs and their corresponding Kirchhoff polynomials (as expression trees). Inherits KirchExpr.
    
    Parameters
    ---------
    name: name of the KirchMDigraph
    edgedata: list of edges and their attributes (default=None). Format: [(v1,v2,{key1:value1, key2:value2, ...}), ...]
    vertrexdata: list of vertices and their attributes (default=None). Format: [(v1,{key1:value1, key2:value2, ...}), ...]
    metadata: information about the KirchMDigraph (default=None)
    arbsType: type of arborescences (in- or out-arborescencses) for which digraph opearions to be performed; options -- 'in' or 'out' (deafault='out')
              NOTE: for 'in' arborescences the input multidigraph is reversed and stored as such since the in-arborescences of any digraph G correspond to the out-arborescences of the reverse (with all adges reversed) graph G^R.
    tokenId: id of the KirchMDigraph (default tokenId=None). Generated at KirchMDigraph initialisation (checks which different KirchMDigraphs in the expression have identical Kirchhoff polynomials have not been performed) and by primeFactoriseInPlace (KirchMDigraphs with identical Kirchhoff polynomials are assigned the same tokenId).
    kwargs: additional attributes for KirchMDigraph
    
    NOTE: A set of uniqe labels is automataically generated corresponding to the edge attribute defined in the uqLabelKey variable (the default uqLabelKey is defined in common.py).
    NOTE: KirchMDigraph is atomic and has no children.
    
    """
    def __init__(self, name, edgedata=None, vertexdata=None, metadata=None, arbsType='out', tokenId=None, **kwargs):
        KirchExpr.__init__(self, name=name)
                    
        self.metadata = metadata

        # KirchMDigraph identifier 
        if tokenId is not None:
            self.tokenId = tokenId
        else:         
            self.tokenId = KirchExpr.nextTokenId()
            
        # composition of KirchMDigraph with networkx multidigraph
        self.mdigraph = nx.MultiDiGraph()
        
        if vertexdata is not None:
            self.mdigraph.add_nodes_from(vertexdata)

        if edgedata is not None and edgedata != []:
            uqLabellededgedata = self.uqLabel(edgedata)
            self.mdigraph.add_edges_from(uqLabellededgedata)

        if kwargs is not None:
            self.attributes = kwargs
        else:
            self.attributes = {}

        assert arbsType in ['in', 'out'], "KirchMDigraph: Neither \'in\' nor \'out\' arborescences!" # two options only, in and out arborescences
        self.arbsType = arbsType

        # when interested in the in-arborescences of KirchMDigraphs, the mdigraph is stored reversed since all operations are defined for out-arborescences.
        if self.arbsType == 'in': 
            self.mdigraph.reverse(copy=False)
        # one can save Kirch pol results here
        self.implicitFormKirchPol = None            
        self.KirchhoffPolynomial = None
        self.numberOfArborescences = None #self.countArbs()
        # TODO: initialize prime factorisation, number of SCCs, number of arbs (or they do not exist), statistics -- if available in the file
        

    def getEdges(self, data=False):
        """getEdges -- return the list of edges.
        
        Parameters
        ----------
        data: also return the attributes of the edges (default data=False)
        """
        return self.mdigraph.edges(data=data)
        
        
    def getVertices(self):
        """getVertices -- return the list of vertices.
        """
        return self.mdigraph.nodes()


    def size(self):
        """size -- number of vertices in the KirchMDigraph.
        """
        return len(self.mdigraph)


    def getNumberOfEdges(self):
        """getNumberOfEdges -- number of edges in the KirchMDigraph.
        """
        return nx.number_of_edges(self.mdigraph)


    def getNumberOfSCCs(self):
        """getNumberOfSCCs -- number of strongly connected components in the KirchMDigraph.
        """
        return nx.number_strongly_connected_components(self.mdigraph)


    def hasEdge(self, sourceV, targetV):
        """hasEdge -- Answers: Does the mdigraph have the edge?
        """
        return self.mdigraph.has_edge(sourceV, targetV)
        
        
    def getPrimeComponents(self):
        """getPrimeComponents -- retrieve the prime components of the KirchMDigraph.
        """
        primeFact = self.primeFactorisation()
                
        if isinstance(primeFact, ZERO):
            return []
        elif isinstance(primeFact, UNIT) or isinstance(primeFact, KirchMDigraph):
            return [self]
        elif isinstance(primeFact, MUL):
            return primeFact.getChildren()
        else:
            raise NotImplementedError('getPrimeComponents: Not implemented for '+ str(type(primeFact)))        
        
        
    def getSCCs(self):
        """getSCCs -- get the strongly connected components in the KirchMDigraph.
        """
        sccs = []
        for s in list(nx.strongly_connected_component_subgraphs(self.mdigraph)):
            currG = self.copy()
            currG.mdigraph = s
            sccs.append(currG)
        return sccs
        
        
    def getMdigraph(self):
        """getMdigraph -- 
        """
        if self.arbsType == 'in':
            G = self.mdigraph.reverse(copy=True)
        else:
            G = self.mdigraph
        return G

        
    # =======================================================
    # Kirchoff polynomial generation/arborescence enumeration
    # =======================================================
    def enumerateArbs(self, edgeAttribute=None, heuristic=None, algorithm=None, reportSteps=None): 
        """enumerateArbs -- returns the Kirchhoff polynomial of self as a KirchExpr or a list of KirchExpr (implicit form).

        Parameters
        ---------
        edgeAttribute: edge attribute to be considered as label during the arborescence enumeration/Kirchhoff polynomial generation; it could be any attribute of the edges ( default is the automatically generated unique label edgeAttribute=uqLabelKey). If edgeAttribute=None the default key is uqLabelKey defined in common.py.
        heuristic:    a heuristic to use for arborescence enumeration of KirchMDigraph objects in case enumerateArbs=True (default heuristic=None). The heuristics are defined in the method getEdgeForHeuristicDeletionContraction of the KirchMDigraph class and are a sequence of four subheuristics, e.g. (1,1,1,1)
        algorithm: algorithms to be used for Kirchhoff polynomial generation (default algorithm='implicit_full').
                   Algorithms:
                       - 'recursive' -- C_R
                       - 'implicit_full'-- C_I with substitutions applied (outputs a single Kirchhoff polynomials)
                       - 'implicit' -- C_I without applying the substitutions (outputs a Kirchhoff polynomial in CompactKirchPol form)
        reportSteps: number of iteration steps after which information about the Kirchhoff polynomial generation is reported; works only for C_I (default reportSteps=None).
        
        Note: The function does not change self.
        Note: If an edge attribute is not present an automatic 'x' will be outputted (defined in *notAssignedEdgeAttribute* in common.py).
                    
        """
        # default algorithm
        if algorithm is None: algorithm = 'implicit_full'
            
        if algorithm == 'recursive': # use C_R
            enum = self.enumerateArbsRecursively(edgeAttribute=edgeAttribute, heuristic=heuristic)
        else: # use C_I (the implicit enumeration algorithm)
            P,R = self.enumerateArbsImplicitly(edgeAttribute=edgeAttribute, heuristic=heuristic, reportSteps=reportSteps)
            enum = KirchMDigraph.assembleImplicitFrom(cmn.rootToken, P.values(), R, algorithm=algorithm)
        return enum.condenseOperationsInExpressionTree()
        
        
    def labelSum(self, edgeAttribute):
        """labelSum -- sum of the labels with (*edgeAttribute* attributes) in digraphs with two or less vertices; every edge is an arborescnece in a digraph with two vertices.
        """
        if self.size() < 2: # single vertex --> trivial arborescence 1
            return UNIT()

        assert self.size() == 2, 'Vertices should be two!'

        edgesData = [ INDET(e[2][edgeAttribute]) 
                      if e[2].has_key(edgeAttribute) 
                      else INDET(cmn.notAssignedEdgeAttribute) 
                      for e in self.mdigraph.edges_iter(data=True) ]
        
        if len(edgesData) == 0:  # no data means that no edge exists in the digraph --> no arborescences
            return ZERO()
        elif len(edgesData) == 1:# single arborescence
            return edgesData[0]
        else:
            return ADD(edgesData, category='labels')        
        
        
#TODO: include also faster code to generate the expressions as strings directly and not as KirchExpr
    def enumerateArbsRecursively(self, edgeAttribute=None, heuristic=None): 
        """enumerateArbsRecursively - returns the Kirchhoff polynomial of *self* as a KirchExpr.

        Parameters
        ---------
        edgeAttribute: edge attribute to be considered as label during the arborescence enumeration/Kirchhoff polynomial generation; it could be any attribute of the edges ( default is the automatically generated unique label edgeAttribute=uqLabelKey). If edgeAttribute=None the default key is uqLabelKey defined in common.py.
        heuristic:    a heuristic to use for arborescence enumeration of KirchMDigraph objects in case enumerateArbs=True (default heuristic=None). The heuristics are defined in the method getEdgeForHeuristicDeletionContraction of the KirchMDigraph class and are a sequence of four subheuristics, e.g. (1,1,1,1)
        
        Note: Does not change self.
        Note: If the edge attribute is not present an automatic 'x' will be outputted (defined in notAssignedEdgeAttribute in common.py).
        
        """
        # === local functions -- recursive algorithm C_R ===
        def enumerateArbsRecurse(G):
            """enumerateArbsRecurse -- recursion routine of the generation.
            """
            # arbs do not exist
            if G.checkArbExistence() == False:
                # TODO: take care of zeroes later when the whole expression is generated!
                return ZERO() # '0'
                
            # base case -- one or two vertices (and arbs exist)
            if G.size() <= 2:
                return G.labelSum(edgeAttribute)

            # TODO: arb existence checked for the second time, but it is needed in primeFactorisation, put primeFactorisation in front?
            prime = G.primeFactorisation()

            if isinstance(prime, KirchMDigraph):# G itself is prime
                return enumerateArbsInPrime(G) # should be unchanged from the prime factorisation!
            else: # is a MUL expression
                MULChildren = prime.getChildren()
                prime.removeChildren()
                for currChild in MULChildren:
                    prime.addChild(enumerateArbsInPrime(currChild))
                return prime
            #TODO: also check if ZERO when not checking existence here
            
            
        def enumerateArbsInPrime(G):
            """enumerateArbsInPrime -- enumerate arborescences in a prime component/factor.
            """
            # base case -- one or two vertices (and arbs always exist in prime factors/components!)
            if G.size() <= 2:
                return G.labelSum(edgeAttribute)
                
            # choose an edge for heuristic deletion-contraction
            edge = G.getEdgeForHeuristicDeletionContraction(heuristic=heuristic)
            
            # delete and contract the edge
            delContrExpr = G.edgeDeletionContraction(edge)
            
            if isinstance(delContrExpr, KirchMDigraph):# only del digraph has arbs
                return enumerateArbsRecurse(delContrExpr) # delContrExpr has one edge less
                
            elif isinstance(delContrExpr, MUL): # only the contracted digraph has arbs
                MULChildren = delContrExpr.getChildren()
                delContrExpr.removeChildren()
                for currChild in MULChildren:                    
                    delContrExpr.addChild(enumerateArbsRecurse(currChild))
                return delContrExpr

            elif isinstance(delContrExpr, ADD): # both deleted and contracted digraphs have arbs
                ADDChildren = delContrExpr.getChildren()
                delContrExpr.removeChildren() 
                for currADDChild in ADDChildren:                    
                    if isinstance(currADDChild, KirchMDigraph): # current child is the edge deleted digraph
                        delContrExpr.addChild(enumerateArbsRecurse(currADDChild))                        
                    elif isinstance(currADDChild, MUL): # the child is the multiplication operation
                        # TODO: optimise by removing the ifs after testing and directly call labelSum for the edge digraphs?
                        MULChildren = currADDChild.getChildren()
                        currADDChild.removeChildren()                        
                        for currMULChild in MULChildren:
                            if isinstance(currMULChild, KirchMDigraph): # current child is the edge contracted digraph
                                currADDChild.addChild(enumerateArbsRecurse(currMULChild))
                            else:
                                raise RuntimeError('enumerateArbsInPrime: Should never occur!')
                        delContrExpr.addChild(currADDChild)
                    else:
                        raise RuntimeError('enumerateArbsInPrime: Should never occur!')

                return delContrExpr

        # === body ===
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)
        Gr = self.copy()
        arbs = enumerateArbsRecurse(Gr)
        return arbs


    def enumerateArbsImplicitly(self, edgeAttribute=None, heuristic=None, reportSteps=None):
        """enumerateArbsNEW -- algorithm C_I incorporating change of variables
        """
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)
        Gr = self.copy()
        Gr.tokenId = cmn.rootToken # root of the enumeration tree        
        Q = cmn.Queue()
        Q.enqueue(Gr) # queue of the non-prime digraphs to go enumerate        
        R = {} # results dictionary; format: {tokenId:KirchExpr} 
        P = {} # dictionary containing the already considered prime components (remembered as the set of their labels); format {<prime comp set>:<tokenID>}
        i = 0

        while not Q.isEmpty():
            currNonPrime = Q.dequeue() # also assumes the initial digraph is not prime
            primeExpr = currNonPrime.primeFactorisation()

            # report the size of the queue and the resulting dictionaries
            if reportSteps is not None:
                i+=1 
                if i%reportSteps == 0:
                    print '|Q|=', Q.size(), '|P|=', len(P), '|R|=', len(R)

            primeComponents = [] # pass list by reference to be filled up with prime components
            if isinstance(primeExpr, KirchMDigraph): # the digraph is already prime, no need to add it to the Results dictionary R
                primeComponents = [primeExpr]
            else:
                indetSubstitutedExpr = primeExpr.getKrichMDigraphsAndSubstWithIndets(primeComponents, edgeAttribute=edgeAttribute) # IMPORTANT: the function also invokes labelSum for two and single vertex digraphs!
                R[currNonPrime.tokenId] = indetSubstitutedExpr # add to results dictionary

            # optimize tree for single prime factors! 
            if primeComponents != []: # There are prime components to further decompose (it could happen that primeExpr contains only UNIT and/or ZERO)
                for C in primeComponents: 
                    if C.size() <= 2: # base case -- relevant when the input digraph is prime and of size <=2
                        R[C.tokenId] = C.labelSum(edgeAttribute)
                    else:
                        # NOTE: Only the sufficient condition is used to certify Kirchhoff polynomial equality, thus some digraphs with equal Kirchhoff polynomials can be omitted;
                        primeCompUniqueEdgeLabels = C.getLineDigraphEdges() # use the automatically generated unique labels for digraph comaprisons, since user inputted values may not be unique; group attributes due to parallel edges.

                        if primeCompUniqueEdgeLabels in P:# enumerated before -- do not enqueue for further enumeration
                            P[primeCompUniqueEdgeLabels][1].append(C.tokenId)                        
                        else: # not enumerated yet -> apply deletion-contraction 
                            P[primeCompUniqueEdgeLabels] = (C.tokenId,[])
                            # TODO: dynamically remove entries from P that cannot be matched to any upcoming digraph 
                            # choose an edge for heuristic deletion-contraction
                            edge = C.getEdgeForHeuristicDeletionContraction(heuristic=heuristic)
                                
                            # delete and contract the edge
                            delContrExpr = C.edgeDeletionContraction(edge)
                            
                            kirchmdigraphs = [] # pass list by reference
                            indetSubstitutedDelContrExpr = delContrExpr.getKrichMDigraphsAndSubstWithIndets(kirchmdigraphs, edgeAttribute=edgeAttribute)# IMPORTANT: the function also invokes labelSum for two and single vertex digraphs!

                            R[C.tokenId] = indetSubstitutedDelContrExpr # add to results dictionary

                            for kdgr in kirchmdigraphs:
                                Q.enqueue(kdgr)                   
        # remove unmatched digraphs
        P = { k:v for k,v in P.iteritems() if v[1]!=[]}                                
        return P,R


    @staticmethod
    def assembleImplicitFrom(startTokenId, Pvals, R, algorithm='implicit_full'):
        """assembleImplicitFrom -- assemble the partial expression trees
        """
        def assemble(currExpr):
            """assemble -- 
            """
            # ASSEMBLE WHOLE EXPRESSION
            if currExpr.checkIfOperation():
                newChildren = [assemble(child) for child in currExpr.getChildren()]
                currExpr.children = newChildren
                return currExpr
            elif currExpr.checkIfAtomic():# leaves that need not be prime factorised/decomposed
                if isinstance(currExpr,INDET):
                    if currExpr.value == cmn.linkVal: # check if this is the INDET storing connections to other digraphs or a leaf of the tree (label if the digraph)
                        currTokenId = currExpr.tokenId
                        if algorithm == 'implicit_full':
                            if currTokenId in R: # first digraph with this set of labels
                                newExpr = R[currTokenId]
                            else:
                                equivalentTokenId = [k for k,v in PvalDict.iteritems() if currTokenId in v][0]
                                newExpr = R[equivalentTokenId]
                            return assemble(newExpr)
                        elif algorithm == 'implicit':
                            if currTokenId in PvalDict:
                                currExpr.value = currTokenId
                                Q.enqueue(currTokenId)
                            else:
                                tokenIdEquivalent = [k for k,v in PvalDict.iteritems() if currTokenId in v]
                                if tokenIdEquivalent != []:
                                    currExpr.value = tokenIdEquivalent[0]
                                else:
                                   newExpr = R[currTokenId]
                                   return assemble(newExpr)
                return currExpr                
            elif isinstance(currExpr, KirchMDigraph):
                return currExpr
            else:
                raise NotImplementedError('assembleCompleteForm: Not implemented for '+ str(type(currExpr)))  


        PvalDict = {v1:v2 for v1,v2 in Pvals}
    
        if algorithm == 'implicit':
            Q = cmn.Queue()
            Q.enqueue(startTokenId) # queue for 
    
            changedVarsDict = {}
            while not Q.isEmpty():
                currTokenId = Q.dequeue() # also assumes the initial digraph is not prime
                changedVarsDict[currTokenId]=assemble(R[currTokenId])
            return cmpct.CompactKirchPol(changedVarsDict=changedVarsDict)
        elif algorithm == 'implicit_full':
            return assemble(R[startTokenId])
        else:
            raise ValueError('assembleImplicitFrom: no such enumeration algorithm.')


# TODO: update when changing the form of P
    @staticmethod
    def genEvaluationDAG(P,R):
        """genEvaluationDAG -- 
        """
        enumEdges = cmn.flatten([ [ (k,x[1]) 
                                        if x[0]==cmn.linkVal 
                                        else (k,x[0]) 
                                            for x in v.getIndeterminates(returnTokenId=True)] 
                                                for k,v in R.iteritems()])
        pointerEdges = cmn.flatten([ [(vv,u) 
                                        for vv in v] 
                                            for u,v in P.values()])
        evalDAGedges = enumEdges + pointerEdges
        return evalDAGedges


    # ===========================
    # Operations on KirchMDigraph
    # ===========================
    # TODO: edge deletion-contraction in place! --> convert the class of the current expression
    def edgeDeletionContraction(self, edge, verbosity=True):
        """edgeDeletionContraction -- apply edge deletion-contraction to KirchMDigarph. Returns the deletion-contraction expression as KirchExpr.

        Parameters
        ---------
        edge: edge to be deleted and contracted
        verbosity: print a message when the edge does not exist or there are no arborescences (default verbosity=True)
        
        Note: Does not change the mdigraph instance.
        Note: If in-arborescences are of interest, the edge needs to be reversed before being passed to the method!

        """            
        if self.checkArbExistence():
            if self.hasEdge(*edge):
                edgeDelGraph   = self.copy()
                contrEdge      = self.copy()
                edgeContrGraph = self.copy()
        
                edgeDelGraph.deleteEdges(edge)
                contrEdge.keepEdge(edge)
                edgeContrGraph.contractEdges(edge)

                # the edge exists, so it cannot be that both edgeDelGraph and edgeContrGraph lack arborescences
                delExist   = edgeDelGraph.checkArbExistence()
                contrExist = edgeContrGraph.checkArbExistence()
    
                if delExist and contrExist:
                    delContrExpression = ADD([edgeDelGraph, MUL([contrEdge, edgeContrGraph], category='delection-contraction')], category='delection-contraction')
                elif delExist:
                    delContrExpression = edgeDelGraph
                elif contrExist:
                    delContrExpression = MUL([contrEdge, edgeContrGraph], category='delection-contraction')
                    
                return delContrExpression
            else: # the edge does not exist in the digraph --> the result is the same digarph
                # TODO: raise exception! always delete-contract an existing edge?
                if verbosity:
                    print 'edgeDeletionContraction: The edge ' + str(edge) + ' does not exist in the digraph!'
                return self.copy() # TODO: keep this?
        else: # no arbs --> edge deletion-contaction cannot be applied
            if verbosity:
                print 'edgeDeletionContraction: No arborescences exist! Deletion-contraction cannot be applied!'
            return ZERO()


    def primeFactorisation(self):
        """primeFactorisation --- obtain the prime/irreducible factorisation/decomposition of the mdigraph.
        
        Note: Does not change the mdigraph instance.
        
        @ authors: Pencho Yordanov, Przemyslaw Uznanski
        """
        # === local functions -- factorisation wrt SCCs and non-trivial dominators ===
        def getDominatorFactors(G):  
            """getDominatorFactors -- get the factorisation/decomposition by considering the vertex domination relationships.
            
            Note: MODIFIES G, but only if G factorises non-trivially.
            """
            # find the root (with zero in-degree) of the digraph factor/prime component
            rootVertices = KirchMDigraph.getInRootVertices(G.mdigraph)
            
            if len(rootVertices) != 1: # strongly connected (since arb existence has already been checked)
                return [G]
        
            [root] = rootVertices
            domTree = G.getDominatorTree(root)
            digraphFactors = []

            def recursiveOnTree(v):
                for predV in domTree.predecessors(v):
                    recursiveOnTree(predV)
                interestingVertices = domTree.predecessors(v)+[v]
                if len(interestingVertices) > 1: #no point in contracting for leaves: [v] -> v
                    domcurrFactor = G.copy()
                    domcurrFactor.mdigraph = G.mdigraph.subgraph(interestingVertices)
                    domcurrFactor.rootAt(v)
                    digraphFactors.append(domcurrFactor)
                    if v != root: #no point in contracting for root, as G is unused
                        G.contractVSubsetToVertex(interestingVertices, v) #G is MODIFIED here!
        
            recursiveOnTree(root)
            return digraphFactors


        def getSCCFactors(G):
            """getSCCFactors -- get SCC digraph factors/components, i.e. a SCC together with its incoming edges, whose source vertices are fused to a single vertex.
            """
            digraphFactors = []
            SCCs = list(nx.strongly_connected_component_subgraphs(G.mdigraph))

            if len(SCCs) == 1:# prime
                return [G]
        
            for SCC in SCCs:
                setVerticesInSCC = set(SCC.nodes())
                
                SCCIncomingEdges = [edge 
                                    for v in SCC.nodes() 
                                    for edge in G.mdigraph.in_edges_iter(v, data=True) 
                                    if edge[0] not in setVerticesInSCC]
                
                if SCCIncomingEdges != []:
                    # id for the fused vertex
                    idVNew = SCCIncomingEdges[0][0]
                    SCC.add_edges_from( (idVNew, edge[1], edge[2]) for edge in SCCIncomingEdges )

                if len(SCC) > 1:# number of vertices in the digraph; do not append trivial factors
                    SCCcurrFactor = G.copy()
                    SCCcurrFactor.mdigraph = SCC
                    digraphFactors.append(SCCcurrFactor)
        
            return digraphFactors

        # === body ===
        Gr = self.copy()

        if Gr.checkArbExistence():
            if Gr.size() == 2: # trivial factorisation/decomposition, every digraph with two vertices is prime (and, at least one edge should be present; arborescences exist, therefore the digraph is connected)
                return Gr
            elif Gr.size() == 1: # single vertex --> the trivial arborescence
                return UNIT()
                
            primeFactors = []
            
            for G1 in getSCCFactors(Gr):
                dFactors = getDominatorFactors(G1) #G1 could be modified here (if len(dFactors)>1), but Gr could be G1
                if len(dFactors) == 1: 
                    primeFactors.append(G1)  # no point in trying once again SCC factors
                else:
                    for G2 in dFactors:
                        primeFactors += getSCCFactors(G2)

            if len(primeFactors) == 1: # the digraph is prime; trivial factorisation/decomposition
                return Gr
            else: # return the product of the non-trivial prime factors/components
                return MUL(primeFactors, category='prime')
                
        else: # no arborescences => return ZERO
            return ZERO()


    # ========================================
    # Arborescence operations on KirchMDigraph
    # ========================================
    def countArbs(self, method='mat'):
        """countArbs -- count arborescences.

        Parameters
        ----------
        method: two alternative ways to count arborescences:
                method='mat' - through cofactors of the Kirchhoff's matrix. NOTE: does not support large integers yet!
                method='pol' - generate the Kirchhoff polynomial and substitute the unique labels with 1, then evaluate the polynomial.
        
        """
        def diagMatrixCofactors(matrix):
            """diagMatrixCofactors -- calculate the array of j,j cofactors of matrix.
            """
            C = np.zeros(matrix.shape[0], dtype=object)
            nels = C.shape[0]
            if nels == 1:
                return [0.]
            for j in xrange(nels):
                minor = matrix[np.array(range(j)+range(j+1,nels))[:,np.newaxis],
                               np.array(range(j)+range(j+1,nels))]    

                # TODO: calculate large determinants of integer matrices!
                det = np.linalg.det(minor)
                sign = (-1)**(j+j)
                C[j] = sign * det
            return C
            
        if self.checkArbExistence():
            if method == 'mat':
                # KIRCHHOFF MATRIX METHOD
                # vertex list
                vList = self.getVertices()                
                # adjacency matrix
                A = nx.adjacency_matrix(self.mdigraph) # default vertex ordering is produced by self.getVertices().
    
                indegsdict = self.mdigraph.in_degree()
                # order indegees wrt vertices. NOTE: taking only the values of hte dictionary may shuffle their order
                indegs = [indegsdict[v] for v in vList]
                D = np.diag(indegs) # diagonal matrix of indegees    
                # Kirchhoff's matrix
                K = D - A
                # number of arborescences
                numArbs = int(np.rint(sum(diagMatrixCofactors(K))))# has to be rounded!
            elif method == 'pol':
                # KIRCHHOFF POLYNOMIAL METHOD -- less efficient 
                countLabel = 'countLabel'
                ledges = [(e[0],e[1],{countLabel:'1'}) for e in self.getEdges()]
                # mdigraph with counting labelled edges
                G = self.copy()
                G.mdigraph = nx.MultiDiGraph(ledges)
                expr = G.generateExpression(enumArbs=True, edgeAttribute=countLabel)
                numArbs = eval(expr)
#                #substitution -- even less efficient
#                expr = sympy.S(self.generateExpression(enumArbs=True))
#                # substitute variables(labels) with ones
#                subs = [(s,1) for s in expr.free_symbols]
#                numArbs = expr.subs(subs)
        else:
            numArbs = 0

        self.numberOfArborescences = numArbs
        return numArbs

        
    def expandedLengthKirchPol(self):
        """expandedLengthKirchPol -- calculates the expanded length of the Kirchhoff polynomial as the number of vertices in its expression tree.
                                     The formula used is: (|arb|*(|V|-1){varaibles in an arborescence, i.e. number of leaf vertices}+|arb|{multiargument multiplication operator}+1{multiargument addition of the arborescences}
                                     Short form: |arb|*|V|+1
        """
#        return self.countArbs()*self.size()+1
        numbArbs = self.countArbs()
        numbVert = self.size()
        numbVariables = numbArbs*(numbVert - 1)
        plusOperator  = 1 if numbArbs > 1 else 0
        multOperators = numbArbs if numbVert > 2 else 0
        return numbVariables + multOperators + plusOperator
        
        
    def checkArbExistence(self):
        """checkArbExistence -- check if arborescences exist in the digraph (checks whether the digraph is connected and has one initial SCC)

        """
        if self.size() == 0: # empty digraph has no arborescences 
            return False
        # if the digraph is not connected, it will fail the test for a single source-type component anyway
        return len(KirchMDigraph.getInRootVertices(nx.condensation(self.mdigraph))) == 1


    def rootAt(self, vertex, networkxGraph=None):
        """rootAt -- removes the incoming edges to vertex (rooting w.r.t. out-arborescences).
        
        Parameters
        ---------
        vertex: a single vertex for the rooting operation
        networkxGraph: apply the rooting operation to a networkx graph and not to the current oblejct instance (default networkxGraph=None)
        
        Note: The function changes self.mdigraph.

        """
        if networkxGraph is not None:
            return networkxGraph.remove_edges_from(networkxGraph.in_edges(vertex))
        else:
            self.mdigraph.remove_edges_from(self.mdigraph.in_edges(vertex))
        
        
    def deleteEdges(self, *edges):
        """deleteEdges -- deletes (multiple) edges from self.mdigraph.

        Parameters
        ---------
        edges: edge/s to be deleted; format [(v1, v2), ...];
        
        Note: The function changes self.mdigraph
        Note: in case of in-arborescences the input edge needs to be reversed beforehand!

        """
        for edge in edges:
            sourceV = edge[0]
            targetV = edge[1]
            if self.hasEdge(sourceV, targetV):
                numEdgesEDG = len(self.mdigraph[sourceV][targetV])
                self.mdigraph.remove_edges_from([(sourceV,targetV)]*numEdgesEDG)
            # else: the edge is not present, i.e. no need to delete it


    def keepEdge(self, edge):
        """keepEdge -- delete everything else apart from *edge*.

        Parameters
        ---------
        edge: the only edge to be kept in the digraph (along with any parallel edges);  format [(v1, v2), ... ];

        NOTE: In case of in-arborescences the input edge needs to be reversed before invoking the method.
        NOTE: Changes the self.mdigraph.

        """
        sourceV, targetV = edge[0], edge[1]
        # get all parallel edges together with their attributes
        self.mdigraph = self.mdigraph.subgraph([sourceV, targetV])
        # remove any multiple edges in the opposite direction, if existing
        if self.hasEdge(targetV, sourceV):
            numEdgesEG = len(self.mdigraph[targetV][sourceV])
            self.mdigraph.remove_edges_from([(targetV,sourceV)]*numEdgesEG)


    def deleteVertices(self, *vertices):
        """deleteVertices -- deletes vertices from self.mdigraph.
        
        Parameters
        ---------
        vertices: vertex/list of vertices to be deleted.
        
        Note: The function changes self.mdigraph.

        """
        self.mdigraph.remove_nodes_from(vertices)


    def contractEdges(self, *edges):
        """contractEdges -- contracts edges.

        Parameters
        ---------
        edges: edge/ list of edges to be contracted; format [(v1, v2), ...];        
        
        Note: If any of the edges to  be contracted does not take part in the digraph (even if the previous contractions are responsible for that) the self changes to ZERO().
        Note: The function changes self.mdigraph.
        """

        if all([True for sV,tV in edges if self.hasEdge(sV, tV)]): # all edges to be contracted exist in the digraph
            # take the unique lables; they uniquely define the edges which are to be contracted; this is required since contractions change vertex names.
            uniqueLabelsContrEdges = [
                                      self.mdigraph[sourceV][targetV][0][cmn.uqLabelKey]
                                      for (sourceV, targetV) in edges
                                      ] 

            for currLabel in uniqueLabelsContrEdges:                
                # find the edge with the label edgeAttribute:edgeLabel
                edge = [
                        (e[0],e[1]) 
                        for e in self.getEdgeAttribute(cmn.uqLabelKey, onlyAttribute=False) 
                        if e[2]==str(currLabel)
                        ] 

                if len(edge) == 1:
                    (sourceV, targetV) = (edge[0][0], edge[0][1]) 
                    self.contractVSubsetToVertex([sourceV, targetV], sourceV)
                elif len(edge) == 0:
# previous contractions have deleted it --> when all contractions are applied the digraph has no arborescences
                    # TODO: keep class conversion or just delete all vertices to obtain the empty digraph that has no arborescences?
#                    self.mdigraph.remove_nodes_from(self.getVertices())
                    self.__class__ = ZERO # CLASS CONVERSION!
                    (self.value, self.name) = ('0', 'zero')
                    break
                else:
                    raise ValueError('contractEdges: multiple edges with the same unique label?!')
        else: # some edge is not present in the digraph -> contractions cannot be applied since there does not exist an arborescence containing all contracted edges -> return delete all 
#            self.mdigraph.remove_nodes_from(self.mdigraph.nodes())
                self.__class__ = ZERO # CLASS CONVERSION!
                (self.value, self.name) = ('0', 'zero')


    def contractVSubsetToVertex(self, S, u):
        """contractVSubsetToVertex -- collapse/contract/condense the vertex subset S to a single vertex u; G(S->u).
            1. All edges xy x \in V\S, y \in S\{u} are removed from G
            2. All edges within S are removed
            3. All vertices in S are contracted into u   
            
        Parameters
        ---------
        S: vertex subset to be collapsed
        u: vertex to which S is collapsed
            
        Note: The function changes self.mdigraph (and changes vertex names).
 
        """
        # vertices to be deleted and for which all incoming edges from the main digraph are deleted
        assert u in S, 'contractVSubsetToVertex: u should be in S!'
        sS = set(S)
        contrVs = sS - set([u])
        for edge in self.mdigraph.out_edges_iter(contrVs, data=True):
            if edge[1] not in sS:
                self.mdigraph.add_edge(u, edge[1], attr_dict=edge[2])
        # remove the vertices from S\{u}
        self.mdigraph.remove_nodes_from(contrVs)


    def relabelEdge(self, edge, newLabel, edgeAttribute=None, parallelEdgeIndex=0):
        """relabelEdge -- 
        Parameters
        ---------
        edge: edge to be relabelled
        newLabel: new label of the edge
        edgeAttribute: the edge attribute of *edge* which is to be relabelled
        parallelEdgeIndex: if there are more than one parallel edge, the index of the edge whose label is to be changed can be selected

        """
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)

        self.mdigraph[edge[0]][edge[1]][parallelEdgeIndex][edgeAttribute] = newLabel



    # ========================================
    # Digraph connectivity properties
    # ========================================
    def getStrongBridges(self):
        """getStrongBridges -- find all strong bridges, i.e. such edges whose deletion increases the number of strongly connected components of self.mdigraph;
                                Naive implementation!
        """
        strongBridges = []
        initialNumberSCCs = nx.number_strongly_connected_components(self.mdigraph)
        
        # TODO: more efficient implementation
        # naive implementation
        for e in set(self.getEdges()):
            edgeDelGraph = self.mdigraph.copy()
            numEdgesEG = len(self.mdigraph[e[0]][e[1]])
            edgeDelGraph.remove_edges_from([(e[0],e[1])]*numEdgesEG)
            deleNumberSCCs = nx.number_strongly_connected_components(edgeDelGraph)
            if initialNumberSCCs < deleNumberSCCs:
                strongBridges.append(e)
                
        return strongBridges
        

#    def getDominaionBridges(self):
#        """getDominaionBridges -- find all domination bridges, i.e. such edges whose deletion increases the depth of the dominator tree\introduces new non-trivial domination relationships ...
#        """
#        #TODO: implement
#        pass
    
    
    def getDominatorTree(self, root):
        """getDominatorTree -- get the tree of immediate dominators of self.mdigraph with respect to a root vertex.

        Parameters
        ---------
        root: root vertex        
        """
        # TODO: in case there is no root, call rootAt(root) for a copy of the digraph; in case these are multiple roots ... decide what to do
        G = self.mdigraph
        immDoms = dom.immediate_dominators(G, root).items()
        domTree = nx.DiGraph( [d for d in immDoms if d[0]!=root] )
        return domTree


    def getEdgeForHeuristicDeletionContraction(self, heuristic=None):
        """getEdgeForHeuristicDeletionContraction -- choose an edge to delete contract in the generation of the Kirchhoff polynomial / enumeration of arborescences

        Parameters
        ---------
        heuristic: heuristic to be used (default=(1,0,0,0))
                        Each heuristic is composed of four subheuristics:
                            i.) Edges E'(G):
                                0. A single randomly selected edge e, E'(G)={e}.
                                1. All edges, E'(G)=E(G).
                                2. Edges participating in the longest simple cycle.
                                3. The |E(G)|/n, n=3 edges participating in the largest number of simple cycles.
                            ii.) Branch:
                                0. Edge deleted digraph, $G\setminus e$.
                                1. Edge contracted digraph, $G/e$.
                                2. Edge deleted and edge contracted digraph, $(G\setminus e,G/e)$.
                            iii.) Components:
                                0. Strongly connected components.
                                1. Prime components.
                            iv.) Optimality criterion:
                                0. Largest number of components.
                                1. Largest component (in terms of number of vertices) is smallest.
                                2. Largest component (in terms of number of edges) is smallest.
                                3. Smallest total complexity (number of arborescences) of the components. 
                                4. Smallest total complexity with the largest (same) number of components.
                                5. Largest number of components with the (same) smallest total complexity.
        """
        if self.size() <= 2:
            raise ValueError('getEdgeForHeuristicDeletionContraction: There are only two vertices in the graph!')        
        
        # Format: (<which set of edges to cosider>, <components>, <optimised quantity>, <optimised branch>)
        largestHeuristicIds = (3, 2, 1, 5);
        if heuristic is None:          
            heuristic = (1,0,0,0) # default heuristic
        else:
            for i,lh in enumerate(largestHeuristicIds):
                if heuristic[i] not in range(lh+1):
                    raise ValueError('getEdgeForHeuristicDeletionContraction: no such heuristic!')

        (setEdges, branch, components, optQtty) = heuristic

        # === Decide which edges to consider === 
        if setEdges == 0: # a random edge --> no need for further considerations
            return random.sample(self.getEdges(), 1)[0]            
        elif setEdges == 1: # all edges
            setConsideredEdges = self.getEdges()
        elif setEdges == 2: # only edges from the longest simple cycle
            l = list(nx.simple_cycles(self.mdigraph))
            l.sort(key=lambda x: -len(x))
            ll = l[0]
            setConsideredEdges = [(ll[-1+i],ll[i]) for i in xrange(len(ll))]
        elif setEdges == 3: # the top numEdges through which pass the largest number of simple cycles
            n = 3
            numEdges = self.getNumberOfEdges()/n
            cycles = list(nx.simple_cycles(self.mdigraph))
            ff = [[(ll[-1+i],ll[i]) 
                    for i in xrange(len(ll))] 
                        for ll in cycles]
            lf = cmn.flatten(ff)
            lc = [[lf.count(x),x] for x in set(lf)]
            lc.sort(key=lambda x: -x[0])
    #        lc.sort(key=lambda x: +x[0])
            setConsideredEdges = [x[1] for x in lc[:numEdges]]

        # === Decide which branch to consider === 
        digrs = []
        for e in setConsideredEdges:
            if branch == 0: # only the edge deletion branch
                edgeDelDiGraph = self.copy()
                edgeDelDiGraph.deleteEdges(e)
                if components == 0: # SCCs
                    edgeDelDiGraphComps = edgeDelDiGraph.getSCCs()
                elif components == 1: # prime components          
                    edgeDelDiGraphComps = edgeDelDiGraph.getPrimeComponents()
                digrs.append((edgeDelDiGraphComps,e))
            elif branch == 1: # only the edge contraction branch
                edgeContrDiGraph = self.copy()
                edgeContrDiGraph.contractEdges(e)
                if components == 0: # SCCs          
                    edgeContrDiGraphComps = edgeContrDiGraph.getSCCs()
                elif components == 1: # prime components          
                    edgeContrDiGraphComps = edgeContrDiGraph.getPrimeComponents()
                digrs.append((edgeContrDiGraphComps,e))
            elif branch == 2: # both branches
                edgeDelDiGraph = self.copy()
                edgeDelDiGraph.deleteEdges(e)
                edgeContrDiGraph = self.copy()
                edgeContrDiGraph.contractEdges(e)
                if components == 0: # SCCs          
                    edgeDelDiGraphComps   = edgeDelDiGraph.getSCCs()
                    edgeContrDiGraphComps = edgeContrDiGraph.getSCCs()
                elif components == 1: # prime components     
                    edgeDelDiGraphComps   = edgeDelDiGraph.getPrimeComponents()
                    edgeContrDiGraphComps = edgeContrDiGraph.getPrimeComponents()
                digrs.append((edgeDelDiGraphComps+edgeContrDiGraphComps,e))
        
        # === Optimise quantity === 
        if optQtty == 0: # max number of components
            numList = [(len(ds),e) for ds,e in digrs]
            return max(numList)[1]
        elif optQtty == 1: # largest (in number of vertices) component is smallest amongst all decompositions
            largestSize = [ (max([c.size() 
                                for c in ds]),e) 
                                    for ds,e in digrs]
            return min(largestSize)[1]
        elif optQtty == 2: # largest (in number of edges) component is smallest amongst all decompositions
            largestSize = [ (max([c.getNumberOfEdges() 
                                for c in ds]),e) 
                                    for ds,e in digrs]
            return min(largestSize)[1]
        elif optQtty == 3: # smallest total complexity of the components
            totComplexity = [ (sum([c.countArbs() 
                                for c in ds]),e) 
                                    for ds,e in digrs]
            return min(totComplexity)[1]
        elif optQtty == 4: # edge giving rise to the decomposition with the smallest total complexity among the decompositions with the highest (same) number of components
            totComplexityNumber = [ (len(ds),sum([c.countArbs() 
                                        for c in ds]),e) 
                                            for ds,e in digrs]
            totComplexityNumber.sort(key=lambda x: (-x[0], x[1]))
            return totComplexityNumber[0][2]
        elif optQtty == 5:# edge giving rise to the decomposition with the highest number of components among the decompositions with the (same) smallest total complexity
            totComplexityNumber = [ (len(ds),sum([c.countArbs() 
                                        for c in ds]),e) 
                                            for ds,e in digrs]
            totComplexityNumber.sort(key=lambda x: ( x[1],-x[0]))
            return totComplexityNumber[0][2]

    
    # ===============
    # Other functions
    # ===============
    @staticmethod
    def getInRootVertices(G): 
        """getInRootVertices -- return vertices whose indegree is zero --> these are roots (of out-arborescences).

        Parameters
        ---------
        G: networkx multidigraph
        """
        assert any([isinstance(G, nx.DiGraph), isinstance(G, nx.MultiDiGraph)]), "getInRootVertices: G should be DiGraph or MultiDiGraph!"
        return [v 
                for v,indeg in G.in_degree().iteritems() 
                if indeg==0]
       

    def uqLabel(self, edgeList, latexFormat=False):
        """uqLabel -- uniquely label the edges of the mdigraph; add an attribute uqLabelKey:<name>id to the edge attribute dictionary (uqLabelKey is defined in common.py)
        """
        if latexFormat:
            return [(v[0],v[1],cmn.mergeDicts(v[2],{cmn.uqLabelKey:'$'+self.name+'_{'+str(i)+'}$'})) 
                    if len(v)==3 
                    else (v[0], v[1], {cmn.uqLabelKey:'$'+self.name+'_{'+str(i)+'}$'}) 
                    for i,v in enumerate(edgeList)]
                    
        return [(v[0],v[1],cmn.mergeDicts(v[2],{cmn.uqLabelKey:self.name+str(i)})) 
                if len(v)==3 
                else (v[0], v[1], {cmn.uqLabelKey:self.name+str(i)}) 
                for i,v in enumerate(edgeList)]


    def getLineDigraphEdges(self, edgeAttribute=None):
        """getLineDigraphEdges -- edges of the line digraph created from *self* (vertices are edge labels with edgeAttribute not edges). Used to compare digraphs which are derived from a single digraph.
        
        Parameters
        ---------        
        edgeAttribute: edge attribute key denoting the edge labels that are used to generate the line digaphs (default edgeAttribute=None)
        
        NOTE: edgeAttribute needs to be a unique label such that the line digraph is defined.
        """
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)

        edges = self.getEdges(data=True)
        
        # group multiple parallel edges
        edges.sort(key = itemgetter(0,1))
        groups = groupby(edges, itemgetter(0,1))
        multGroupedDict = {key:frozenset([item[2][edgeAttribute] 
                                for item in data]) 
                                    for (key, data) in groups}
        
        # create a digraph for easier analysis of the line digraph
        DG = nx.DiGraph()
        DG.add_edges_from(multGroupedDict.keys())
        
        # get the line digraph of the created digraph
        LDG = nx.line_graph(DG)
        return frozenset([ (multGroupedDict[u],multGroupedDict[v]) for u,v in LDG.edges()])


    def getEdgeAttribute(self, edgeAttribute=None, onlyAttribute=True, groupedAttributes=False):
        """getEdgeAttribute -- retrieve edge attribute values for dictionary key edgeAttribute.
        
        Parameters
        ---------        
        edgeAttribute: edge attribute key (default edgeAttribute=None)
        onlyAttribute: return only the attribute of the edge corresponding to the key edgeAttribute and not the vertices defining the edge (default onlyAttribute=True)
        groupedAttributes: group attributes corresponding to parallel edges in frozensets (default groupedAttributes=False)
        """
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)

        if groupedAttributes:
            seq = self.getEdges(data=True)
            seq.sort(key = itemgetter(0,1))            
            groups = groupby(seq, itemgetter(0,1))
            return frozenset([frozenset([item[2][edgeAttribute] 
                                for item in data]) 
                                    for (key, data) in groups])
        
        return sorted([ ((e[2][edgeAttribute]
                        if onlyAttribute 
                        else (e[0],e[1],e[2][edgeAttribute])) 
                            if e[2].has_key(edgeAttribute) 
                            else cmn.notAssignedEdgeAttribute) 
                                for e in self.getEdges(data=True) ])
        
        
    def plot(self, edgeAttribute=None):
        """plot -- plot the labelled mdigraph with NetworkX.
        
        NOTE: temporarily does not return a result
        TODO: migrate to new networkx and matplotlib!
        
        Parameters
        ---------
        edgeAttribute: The key of the attribute that is to be printed as edge labels (default: the automatically generated unique labels uqLabelKey)

        """
        
        #TODO: migrate to new networkx and matplotlib!
        return
    
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)
        
        # if in-arborescences are of interest, reverse back the edges of the graph since it is stored reversed
        G = self.getMdigraph()
        
        pos = nx.spring_layout(G)
        nx.draw(G, pos=pos)
        nx.draw_networkx_labels(G, pos=pos)
        edgeLabels = {}  # dictionary of node tuples to edge labels: {(nodeX, nodeY): aString}
        
        for a, b in G.edges(): # loop over all the edges
            edat = G.get_edge_data(a, b)
            edgeLabels[(a, b)] = cmn.comma.join([ edat[i][edgeAttribute] 
                                                  if edgeAttribute in edat[i].keys() 
                                                  else ''  
                                                  for i in xrange(len(edat))])   # retrieve the edge data dictionary            
            
        nx.draw_networkx_edge_labels(G, pos=pos, edge_labels=edgeLabels) # draw the edge labels
        plt.show()



    #==============================================================================
    # # PyPlot visualisation
    #==============================================================================
    def plotPyPlot(self, FileName, edgeAttribute=None, decompLabels=None):
        """plotPyPlot -- plot digraph using GraphViz
        
        NOTE: temporarily does not return a result
        TODO: migrate to new pyplot!
        
        Parameters
        ---------
        edgeAttribute: The key of the attribute that is to be visualised as edge label(default: the automatically generated unique labels uqLabelKey)
        decompLabels: labels  -- dict - labels frozen set:colour id
        """
        # pydot functions: http://nullege.com/codes/search/pydot.Dot.create_pdf
        #   dot attributes: https://github.com/erocarrera/pydot/blob/master/pydot.py
        #   dot to latex: http://brighten.bigw.org/projects/ladot/ https://dot2tex.readthedocs.org/en/latest/

        #TODO: migrate to new pyplot!
        return

        def prepareGraphForGraphViz():
            # generate unique colours for the decomposition
            if decompLabels != None:# NB: components should be less than the available svg colours
                compColours = cmn.svgColours[:len(decompLabels)]
                
            # prepare edge attributes
            for u,v,dat in G.edges_iter(data=True):
                # TODO: include when visualising differential systems
                # label edges
                if edgeAttribute!='label':
                    eLabel = dat[edgeAttribute]
                    G.edge[u][v]['label'] = eLabel
                
                # color edges
                if decompLabels != None:
                    for comp,i in decompLabels.iteritems():
                        if eLabel in comp:
                            G.edge[u][v]['color'] = compColours[i] # problrm

            for v,dat in G.nodes_iter(data=True):
                G.node[v]['shape']='circle'
                G.node[v]['fixedsize']='true'

                envVert = dat.get(cmn.envVertexKey, '0')

                # the synthesis/degradation vertex is marked with a symbol 
                if envVert != '0':
                    G.node[v]['label'] = '&#8709;'#"Ø"
                    G.node[v]['fontsize'] = "32"
                    # G.node[v[0]]['width'] = "0.00001"
                    G.node[v]['shape']='none'
        
        
        edgeAttribute = cmn.assignEdgeAttribute(edgeAttribute)

        # add parallel edges!
        G = cmn.multidigraphToDigraph(self.getMdigraph())
        
        # prepare digraph for visualization        
        prepareGraphForGraphViz()

        # convert to pydot graph 
        D = nx.drawing.nx_pydot.to_pydot(G)
        
        D.set_graph_defaults(sep='1',overlap = 'scalexy')#,splines='True')#,nodesep='.75',)
        nx.drawing.nx_pydot.write_dot(G, FileName+'.dot')
    #    D.set_edge_defaults(labelangle='75')
    #    D.set_style(size='109,10')
        D.write_svg(FileName+'.svg',prog='neato')
        D.write_png(FileName+'.png',prog='neato')
        D.write_pdf(FileName+'.pdf',prog='neato')

#        # convert svg to ipe
#        svg = svgtoipe.Svg(FileName+'.svg')
#        svg.parse_svg(FileName+'.ipe')

#        png_str = D.create_png()
#        sio = StringIO() # file-like string, appropriate for imread below
#        sio.write(png_str)
#        sio.seek(0)
#        
#        img = mpimg.imread(sio)
#        plt.imshow(img)