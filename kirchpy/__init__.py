#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
© 2017, ETH Zurich, Pencho Yordanov

KirchPy is a Python library for efficient manipulation and compact generation of Kirchhoff polynomails of directed graphs.
"""
