# -*- coding: utf-8 -*-
"""
© 2017, ETH Zurich, Pencho Yordanov

Module containing the common variables, dictionaries, and functions used in the various classes of kirchexpr.py, as well as general purpose functions.
"""
import os
import networkx as nx


# === FILE ===
fileExtension            = '.kpy'       # extension of KirchPy files
defaultSeparator         = '\t'         # entry separator in a line
newLine                  = '\n'         # new line in file
comment                  = '#'          # comment symbol
comma                    = ','          # comma

# === ATTRIBUTES ===
uqLabelKey               = 'uqlabel'    # key for the unique label
defaultLabelKey          = 'label'      # --- 
envVertexKey             = 'envVertex'  # key for the environment vertex in open systems
notAssignedEdgeAttribute = 'x'          # this value is assigned to defaultLabelKey for edges without user inputted label
# NOTE: in- and out-arborescences are hardcoded as 'in' and 'out', respectively

# === Fixed nomenclature ===
functionTag              = 'K'          # Kirchoff polynomials which are not generated are repsented as functions of the labels of their corresponding digraphs, e.g. K1(x,y,z) would be the function representation of the Kirchoff polynomial of a digraph with edge labels x, y, and z.
tokenName                = 'psi'        # index the KirchMDigraph objects to later easier assign function names to them (tokenName is prepended to the indices)
rootToken                = 'start'      # marks the root of a Kirchhoff polynomial in an implicit (unsubstituted representation) obtained by the C_I algorithm
linkVal                  = 'link'       # marks places where change of variables in a Kirchhoff polynomial was applied
integrationConstant      = 'Const'      # integration constant name


# === DICTIONARIES ===
# common operators, compatible with SymPy
defaultOps = {'plus':'+',
             'minus':'-',
             'times':'*',
             'division':'/',
             'open bracket':'(',
             'close bracket':')',
             'open function':'(',
             'close function':')',
             'derivative':'diff',
             'integral':'integrate',
             'power':'**',
             'eqSeparator':comma,
             'subst':'='}
 
# TODO: complete for other langauages, e.g. Kodiak and Latex
# operators dictionary
opDict = {'default':defaultOps,
         'Matlab':{'plus':          defaultOps['plus'],
                   'minus':         defaultOps['minus'],
                   'times':         '.*',
                   'division':      './',
                   'open bracket':  defaultOps['open bracket'],
                   'close bracket': defaultOps['close bracket'],
                   'open function': defaultOps['open bracket'],
                   'close function':defaultOps['close bracket'],
                   'derivative':    'diff',
                   'derivative':    'int',
                   'power':         '^'
                         },
         'Mathematica':{'plus':          defaultOps['plus'],
                        'minus':         defaultOps['minus'],
                        'times':         defaultOps['times'],
                        'division':      defaultOps['division'],
                        'open bracket':  defaultOps['open bracket'],
                        'close bracket': defaultOps['close bracket'],
                        'open function': '[',
                        'close function':']',
                        'derivative':    'D',
                        'integral':      'Integrate',
                        'power':         '^',
                        'eqSeparator':   comma,
                        'subst':         '->'} 
#         'Kodiak':{'plus':defaultOps['plus']
#                         },
#         'Latex':{'plus':defaultOps['plus']
#                         }
                         }


# IMPORTANT: when changing the tags/introducing new ones, they also have to be updated in the method initializeFromFile 
# Tags within .kpy files
tags = {'expressionNameTag':       'EXPRNAME',
        'expressionTag':           'EXPR',
        'expressionAttributesTag': 'EXPRATTRIBUTES',    
        'nameTag':                 'NAME', 
        'tokenIdTag':              'TOKENID', 
        'metaTag':                 'META', 
        'arbsTag':                 'ARBS', 
        'attributesTag':           'ATTRIBUTES', 
        'edgeTag':                 'EDGE', 
        'vertexTag':               'VERTEX', 
        'endTag':                  'END'
        }


## tags within .kpy files used for expressions of Kirchhoff polynomails (EXPR), differential systems
#systemTags = {'parameterBoundsTag':     'bounds',
#              'rateConstLabelTag':      'rateconst',
#              'doseVarTag':             'dose',
#              #'ssOutputVertexWeightTag':'weight',
#              'outputVertexTag':        'output',
#              'environmentVertexTag':   'environment'}
#              
#true              = '1'  # how true is denoted in the file
#conservationConst = 'xT' # conservation constant
#correspCoord      = 'h'  # correspondence coordinate
#
## how to generate the steady-state: 1: generate the compressed form of the steady-state expression; 2: generate the dose edge coefficients
#ssModes = {'compr':'ssCompressed',
#           'coeffs':'ssCoefficients'}

# visualisation settings:
svgColours = ['blue','green', 'red','yellow', 'purple','cadetblue','chartreuse','chocolate','magenta','brown','coral','cornflowerblue','cornsilk', 'crimsoncyan', 'darkblue', 'darkcyan', 'darkgoldenrod', 'darkgraydarkgreen',  'darkkhaki', 'darkmagenta', 'darkolivegreen','darkorange', 'darkorchid', 'darkred', 'darksalmon', 'darkseagreen','darkslateblue', 'darkslategray', 'darkslategrey', 'darkturquoise', 'darkvioletdeeppink', 'deepskyblue', 'dimgray', 'dimgrey', 'dodgerbluefirebrick', 'forestgreen', 'fuchsia', 'gainsboro', 'goldenrod', 'gray', 'greenyellow', 'hotpink', 'indianredindigo', 'khaki', 'lavender', 'lavenderblush','lawngreen', 'lemonchiffon', 'lightblue', 'lightcoral', 'lightcyan', 'lightgoldenrodyellow', 'lightgray', 'lightgreen', 'lightgrey', 'lightpinklightsalmon', 'lightseagreen', 'lightskyblue', 'lightslategray', 'lightslategreylightsteelblue', 'lightyellow', 'lime', 'limegreen', 'linen', 'maroon', 'mediumaquamarine', 'mediumblue', 'mediumorchid','mediumpurple', 'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise','mediumvioletred', 'midnightblue', 'mintcream', 'mistyrose', 'moccasin','navajowhite', 'navy', 'olive', 'olivedraborange', 'orangered', 'orchid', 'palegoldenrod', 'palegreen','paleturquoise', 'palevioletred', 'papayawhip', 'peachpuff', 'perupink', 'plum', 'powderblue', 'redrosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrownseagreen', 'sienna', 'silver', 'skyblue','slateblue', 'slategray', 'slategrey',  'springgreen','steelblue', 'tan', 'teal','blueviolet', 'gold','burlywood',  'thistle', 'tomatoturquoise', 'violet', 'wheat', 'yellow','aquamarine',  'beige', 'yellowgreen','blanchedalmond','bisque','grey']
#histogramBins = 20

# === GENERAL PURPOSE FUNCTIONS === 

def invertDict(d):
    """invertDict -- invert a dictionary.
    """
    inv_map = {}
    for k, v in d.iteritems():
        inv_map[v] = inv_map.get(v, [])
        inv_map[v].append(k)
    return inv_map


def mergeDicts(*dict_args):
    """mergeDicts -- shallow copy and merge into a new dictionary.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def flatten(list_of_lists):
    """flatten -- flatten a list of lists.
    """
    if all([isinstance(el, list) for el in list_of_lists]): # make sure the input is indeed a list of lists
        return [val 
                for sublist in list_of_lists 
                for val in sublist]
    else:
        return list_of_lists


def flattenRecursively(S):
    """flattenRecursively -- flatten recursively a list
    """
    if S == []:
        return S
    if isinstance(S[0], list):
        return flattenRecursively(S[0]) + flattenRecursively(S[1:])
    return S[:1] + flattenRecursively(S[1:])


def flattenDict(d):
    """flattenDict -- flatten a dictionary.
    """
    return flatten([[i, str(d[i])] for i in d])


def addVectors(listVs):
    """addVectors -- add multiple lists elementwise.
    
    Parameters
    ----------
    listVs: list of lists of the same size to be added
    """
    return [sum(x) 
            for x in zip(*listVs)]


# === SPECIFIC FUNCTIONS  === 
def assignEdgeAttribute(edgeAttribute):
    """assignEdgeAttribute -- return the default edge attribute value uqLabelKey in case edgeAttribute==None.
    """
    if edgeAttribute is None:
        return uqLabelKey
    else:
        return edgeAttribute


def genKirchPyInputFromFile(inputFile, separator=None, arbs=None, environmentVertex='O'):
    """genKirchPyInputFromFile -- generate a file in the KirchPy format from a file containing digraph edges (and labels). Returns the name of the generated file.
    
    Parameters
    ----------
    inputFile:         file containing digraph edges; format: every line is an edge <vertexName1><separator><vertexName2>(<separator><label>)\n
    arbs:              type arborescence 'in' or 'out' (default arbs = 'out')
    separator:         separator of the different entries on one line in the input file (default separator='\t')
    environmentVertex: string denoting the environment vertex (default environmentVertex='O')
    
    NOTE: The converted file has the same name as inputFile but has the .kpy extension.
    
    """
    
    def genLine(l, endWithNewLine=True):
        """genLine -- generate a 'defaultSeparator' separated line for the converted file.
        """
        if endWithNewLine:
            return defaultSeparator.join(l + [newLine])
        else:
            return defaultSeparator.join(l)
            
    if separator is None:
        separator = defaultSeparator # default separator -- tab separated entries
    if arbs is None:
        arbs = 'out'                 # default arborescence -- out-arborescences

    # read edges from file
    # '#' marks comments that are not read from the file
    dat = [line.split(separator) 
           for line in open(inputFile)
           if line[0]!=comment and line!=newLine] 
   
    crudeEdges = [e[0:len(e)-1] + [e[-1].rstrip()] for e in dat]
    vertices   = set(flatten([[e[0],e[1]] for e in crudeEdges]))
    
    # write to KirchPy format
    expr = genLine([comment+tags['expressionNameTag'], 'defaultName']) +\
           genLine([comment+tags['expressionTag'], 'default']) +\
           genLine([comment+tags['expressionAttributesTag'], 'defaultKey', 'defaultValue']) +\
           comment +\
           tags['endTag'] + newLine + newLine
           
    head = genLine([tags['nameTag'], 'G']) +\
           genLine([tags['arbsTag'], arbs]) +\
           genLine([comment+tags['tokenIdTag'], tokenName + '0']) +\
           genLine([tags['metaTag'], 'converted from ' + inputFile]) +\
           genLine([comment+tags['attributesTag'], 'defaultKey', 'defaultValue'])
           
    edges = ''    
    for e in crudeEdges:
        if len(e) < 2:
            raise ImportError('At least two entries are needed to define an edge!')
        else:
            currEdge = genLine([tags['edgeTag'],e[0],e[1]], False)
            currAttributes = ''
            if len(e)>2:
                currAttributes = genLine([currEdge, defaultLabelKey,e[2]]+e[3:], False)
            if len(e)==2: # only edges with no attributes
                currAttributes = genLine([currEdge], False)
            edges += currAttributes + newLine

    tail = ''
    if environmentVertex in vertices:
        tail += genLine([tags['vertexTag'], environmentVertex, envVertexKey, '1'])
        
    tail += genLine([comment+tags['vertexTag'], 'defaultVertex', 'defaultKey', 'defaultValue']) +\
            tags['endTag']

    # change extension for the output file
    outputFile = os.path.splitext(inputFile)[0] + fileExtension

    with open(outputFile, 'w') as f:
        f.write(expr + head + edges + tail)
    
    # returns the name of the output file together with file extension
    return outputFile


def multidigraphToDigraph(G, appearance='default'): 
    """multidigraphToDigraph -- convert 
    """
    newG = nx.DiGraph()
    newG.add_nodes_from(G.nodes(data=True))

    newEdges = []
    for u,v in set(G.edges()):
        dat = G.edge[u][v]
        if len(dat) == 1:
            newEdges.append((u,v,dat[0]))
        else:
            super_dict = {}
            for d in dat.values():
                for k, vvv in d.iteritems():
                    super_dict.setdefault(k, []).append(vvv)

            for k, vv in super_dict.iteritems():
                super_dict[k] = opDict[appearance]['plus'].join(vv)
                newEdges.append((u,v,super_dict))
    newG.add_edges_from(newEdges)
    return newG
    
    
class Queue:
    """Queue -- generic queue class.
    """
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def enqueue(self, item):
        self.items.insert(0,item)

    def dequeue(self):
        return self.items.pop()

    def size(self):
        return len(self.items)
    