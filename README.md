# *KirchPy*

*KirchPy* is a Python library for efficient manipulation and compact generation of (expressions of) Kirchhoff polynomials of directed graphs. It implements the theory and algorithms described in [1] and [2].
The package enumerates rooted directed spanning trees (arborescences) and allows the symbolic steady-state analysis of linear diffusion processes on directed graphs. Specifically, it has been employed to study the non-equilibrium steady states of linear framework models in biology [2], [3] and, in general, it enables the efficient application of the Matrix Tree Theorem in the context of Laplacian dynamics and the Markov Chain Tree Theorem in the context of continuous time, finite state space Markov processes.

## Software requirements
*KirchPy* has been tested with Python 2.7 and NetworkX 1.11.0.

## Known issues
Incompatibility with Python 3.x and NetworkX 2.x.

## Installation

Install `pip` and `setuptools`,  go to the folder containing `setup.py`, and type:

```bash
pip install .
```

To uninstall *KirchPy* type:

```bash
pip uninstall kirchpy
```

## Download

*KirchPy* can be downloaded from [https://gitlab.com/csb.ethz/KirchPy]()

## License

*KirchPy* is provided under the BSD-3-Clause license.

## Authors

© 2017, ETH Zurich, Pencho Yordanov

	Pencho Yordanov <yordanov.pencho@gmail.com>

## Citation
To cite *KirchPy* in publications use:

```bibtex
@Article{yordanov2020efficient,
  author   = {Yordanov, Pencho and Stelling, Jörg},
  journal  = {Journal of The Royal Society Interface},
  title    = {Efficient manipulation and generation of Kirchhoff polynomials for the analysis of non-equilibrium biochemical reaction networks},
  year     = {2020},
  number   = {165},
  pages    = {20190828},
  volume   = {17},
  doi      = {10.1098/rsif.2019.0828},
  url      = {https://royalsocietypublishing.org/doi/abs/10.1098/rsif.2019.0828}
}

```

## References
[1] Mihalák, M., P. Uznański, and Yordanov, P. (2016). Prime factorization of the Kirchhoff polynomial: Compact enumeration of arborescences. In ANALCO, pages 93–105. https://doi.org/10.1137/1.9781611974324.10.

[2] Yordanov, P.  and Stelling, J. (2020). Efficient manipulation and generation of Kirchhoff polynomials for the analysis of non-equilibrium biochemical reaction networks. In J. R. Soc. Interface. https://doi.org/10.1098/rsif.2019.0828

[3] Yordanov, P.  and Stelling, J. (2018). Steady-State Differential Dose Response in Biological Systems. In Biophysical Journal. https://doi.org/10.1016/j.bpj.2017.11.3780

[1]: https://doi.org/10.1137/1.9781611974324.10	"Mihalák, M., P. Uznański, and Yordanov, P. (2016). Prime factorization of the Kirchhoff polynomial: Compact enumeration of arborescences. In ANALCO, pages 93–105."

[2]: https://doi.org/10.1098/rsif.2019.0828	"Yordanov, P.  and Stelling, J. (2020). Efficient manipulation and generation of Kirchhoff polynomials for the analysis of non-equilibrium biochemical reaction networks. In J. R. Soc. Interface."

[3]: https://doi.org/10.1016/j.bpj.2017.11.3780	"Yordanov, P.  and Stelling, J. (2018). Steady-State Differential Dose Response in Biological Systems. In Biophysical Journal."
