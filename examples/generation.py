# -*- coding: utf-8 -*-
"""
© 2017, ETH Zurich, Pencho Yordanov
Generating Kirchhoff polynomials with KirchPy.
"""
from kirchpy.kirchexpr import *
import kirchpy.common as cmn

inputFileName = 'models/actinmyosin.txt'
separator     = '\t'
arborescecnes = 'in'
kpyFileName   = cmn.genKirchPyInputFromFile(inputFileName, separator=separator, arbs=arborescecnes)
G = KirchMDigraph('G')
G.initializeKirchExprFromFile(kpyFileName)

print G.expandedLengthKirchPol()

l1 = G.enumerateArbs(heuristic=(1,1,1,3)).getCompressedLength()
l2 = G.enumerateArbs(heuristic=(3,0,0,3)).getCompressedLength()
print l1, l2

a1 = G.enumerateArbs(algorithm='recursive',heuristic=(3,0,0,3)).getCompressedLength()
a2 = G.enumerateArbs(algorithm='implicit_full',heuristic=(3,0,0,3)).getCompressedLength()
a3 = G.enumerateArbs(algorithm='implicit',heuristic=(3,0,0,3)).getCompressedLength()
print a1, a2, a3

print G.enumerateArbs(algorithm='implicit_full',heuristic=(3,0,0,3),edgeAttribute='label').generateExpression()
print G.enumerateArbs(algorithm='implicit',heuristic=(3,0,0,3),edgeAttribute='label').generateExpression()