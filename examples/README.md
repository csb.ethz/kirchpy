# Tutorial

This short tutorial explains the code from `getting_started.py`, `manipulation.py`, and `generation.py`. Folder `models/` contains the necessary graph definitions.

## Getting started with *KirchPy*

We demonstrate the main functionalities of *KirchPy* through a simple model of the nuclear translocation of the nuclear factor of activated T cells (NFAT) from [1].

First, import the main *KirchPy* module `kirchexpr` and the module with auxiliary functions `common`.

```python
>>> from kirchpy.kirchexpr import *
>>> import kirchpy.common as cmn
```

Convert a tab-separated file containing the edges and labels of a directed graph model to *KirchPy* format (with extension `.kpy`).

```python
>>> inputFileName = 'models/NFAT.txt'
>>> separator     = '\t'
>>> arborescecnes = 'in'
>>> kpyFileName = cmn.genKirchPyInputFromFile(inputFileName, separator=separator, arbs=arborescecnes)
```

Note that we declared our interest in working with in-arborescences, i.e. rooted directed spanning trees in which a directed path exists from each vertex to a single root vertex.

The input tab-separated file `models/NFAT.txt` has the format:

> \# Nuclear translocation of the nuclear factor of activated T cells\
> Nc*	Nc	r1\
> Nc	Nc*	r2\
> Nc	Nn	r3\
> Nn	Nc*	r4

and the generated *KirchPy* file  `models/NFAT.kpy` has the format:

> NAME	G\
> ARBS	in\
> META	converted from models/NFAT.txt\
> EDGE	Nc*	Nc	label	r1\
> EDGE	Nc	Nc*	label	r2\
> EDGE	Nc	Nn	label	r3\
> EDGE	Nn	Nc*	label	r4\
> END

Next, instantiate an empty `KirchMDigraph` with name `'G'` and initialize it with the generated `kpyFileName`.

```python
>>> G = KirchMDigraph('G')
>>> G.initializeKirchExprFromFile(kpyFileName)
```

### Derive the steady-state expression for the species `Nn` considering `G` as a closed linear framework model. 

The steady state $`x_{\text{Nn}}^{SS}`$ of a species corresponding to vertex `Nn` in a closed linear framework model can be obtained as a ratio of Kirchhoff (spanning tree) polynomials: 

```math
x_{\text{Nn}}^{SS}=\frac{\kappa(\mathsf{rt}_{\text{Nn}}(G))}{\kappa(G)}x_T,
```

where $`x_T`$ is a conservation constant, $`\kappa(G)`$ is the Kirchhoff polynomial of `G`, $`\mathsf{rt}`$ is a graph rooting operation, and $`\kappa(\mathsf{rt}_{\text{Nn}}(G))`$ denotes the Kirchhoff polynomial of `G` rooted at vertex `Nn`, i.e. all spanning trees of `G` rooted at `Nn`.

To implement the formula we first obtain a copy of `G` and modify it with the rooting operation `rootAt` to derive the numerator of the expression.

```python
>>> Gnum = G.copy()
>>> Gnum.rootAt('Nn')
```

Now we are ready to formulate the steady-state expression as a ratio of Kirchhoff polynomials multiplied by the conservation constant.

```python
>>> xSSNn = MUL([FRAC(Gnum, G), INDET('xT', name='total conserved')]) # Note that the total conserved xT is implemented as a variable/indeterminate
```

The formulated expression can be generated in a string form in which non-generated Kirchhoff polynomials corresponding to directed graphs are represented as functions of their edge labels.

```python
>>> print xSSNn.generateExpression(edgeAttribute='label')

(K1(r1,r2,r3))/(K0(r1,r2,r3,r4))*xT
```

One can factorize the non-generated Kirchhoff polynomials (equivalently, prime decompose their directed graphs) to obtain the coarse-grained representation of the expression.

```python
>>> xSSNn.primeFactoriseInPlace()
>>> print xSSNn.generateExpression(edgeAttribute='label')

(K2(r1)*K1(r3))/(K0(r1,r2,r3,r4))*xT
```

Note that every invocation of `generateExpression` changes the names of the functions. Thus function names are only comparable within an expression.

We see that the denominator of $`x_v^{SS}`$ is prime (as expected for closed models) implying that we cannot simplify the expression.

```python
>>> xSSNnSimpl = xSSNn.simplify()
>>> print xSSNnSimpl.generateExpression(edgeAttribute='label')

(K2(r1)*K1(r3))/(K0(r1,r2,r3,r4))*xT
```

Since we cannot manipulate the coarse-grained form of the expression further, let us generate the explicit form of the expression of Kirchhoff polynomials.

```python
>>> print xSSNn.generateExpression(enumArbs=True)

(G0*G2)/(((G1+G2)*G3+G0*(G2+G3)))*xT

>>> print xSSNn.generateExpression(edgeAttribute='label', enumArbs=True)

(r1*r3)/(((r2+r3)*r4+r1*(r3+r4)))*xT

>>> xSSNn.enumArbsInPlace(edgeAttribute='label')
>>> print xSSNn.generateExpression()

(r1*r3)/((r2+r3)*r4+r1*(r3+r4))*xT
```

The option `enumArbs=True` indicates that all Kirchhoff polynomials in the expression are explicitly generated (arborescences/spanning trees of the corresponding directed graphs are enumerated).  Notice that when `edgeAttribute` is not set, expressions are generated using automatically generated unique labels as variables of the Kirchhoff polynomials. The third set of commands presents an alternative way to generate the Kirchhoff polynomials in an expression which changes the expression `xSSNn`.

### Derive the ratio between the steady-states of  `Nc*` and  `Nc`.

The ratio of steady-states of the species corresponding to vertices `Nc*` and  `Nc`  reads: 

```math
\frac{x_{\text{Nc*}}^{SS}}{x_{\text{Nc}}^{SS}}=\frac{\frac{\kappa(\mathsf{rt}_{\text{Nc*}}(G))}{\kappa(G)}x_T}{\frac{\kappa(\mathsf{rt}_{\text{Nc}}(G))}{\kappa(G)}x_T},
```

which can be encoded in *KirchPy* as:

```python
>>> GnumNcs = G.copy()
>>> GnumNcs.rootAt('Nc*')
>>> xSSNcs = MUL([FRAC(GnumNcs, G), INDET('xT', name='total conserved')])
>>> GnumNc = G.copy()
>>> GnumNc.rootAt('Nc')
>>> xSSNc = MUL([FRAC(GnumNc, G), INDET('xT', name='total conserved')]
>>> ratio = FRAC(xSSNcs, xSSNc)
>>> ratioOriginal = ratio.copy() # keep a copy of the expression before manipulating it (details later)
```

The formulated expression can then be simplified (in its coarse-grained form) and explicitly generated.

```python
>>> ratio = ratio.simplify()
>>> print ratio.generateExpression(edgeAttribute='label', enumArbs=True)

((r2+r3))/(r1)
```

The non-simplified expression is longer than the original one but equal to it. 

```python
>>> print ratioOriginal.generateExpression(edgeAttribute='label',enumArbs=True)

(((r2+r3)*r4)/(((r2+r3)*r4+r1*(r3+r4)))*xT)/((r4*r1)/(((r2+r3)*r4+r1*(r3+r4)))*xT)

>>> print ratio.isEqual(ratioOriginal)

True
```

`isEqual` automatically applies prime decomposition, thus the compared expressions need not be decomposed with `primeFactoriseInPlace` beforehand.

By default `isEqual` does not generate Kirchhoff polynomials and uses the coarse-grained representation for comparison. Therefore, when expressions with generated and coarse-grained Kirchhoff polynomials are compared, the correct comparison cannot be accomplished. To compare an expression with generated Kirchhoff polynomials to an expression in the coarse-grained representation use the option `generateKirchhoffPols=True`. 

```python
>>> print ratio.isEqual(ratioOriginal.enumArbsInPlace())

False

>>> print ratio.isEqual(ratioOriginal.enumArbsInPlace(), generateKirchhoffPols=True)

True
```

Finally, the expressions can be written to a file

```python
>>> resultsFileName = 'models/results'
>>> ratio.writeKirchExprToFile(resultsFileName, edgeAttribute='label')
```

producing `'models/results.kpy'`:

> \#kpy file  
> EXPRNAME	expr\
> EXPR	K1(r2, r3)/K0(r1)\
> END
>
> NAME	G\
> ARBS	in\
> TOKENID	K1\
> META	converted from models/NFAT.txt\
> EDGE	Nc	Nc*	label	r2	uqlabel	G1\
> EDGE	Nc	Nc*	label	r3	uqlabel	G2\
> VERTEX	Nc*\
> VERTEX	Nc\
> \#|V|:	2\
> \#|E|:	2\
> \#|SCC|:	2\
> \#|Arbs|:	2\
> \#Expanded length:	3\
> \#Kirchhoff polynomial:	None\
> END
>
> NAME	G\
> ARBS	in\
> TOKENID	K0\
> META	converted from models/NFAT.txt\
> EDGE	Nc*	Nc	label	r1	uqlabel	G0\
> VERTEX	Nc*\
> VERTEX	Nc\
> \#|V|:	2\
> \#|E|:	1\
> \#|SCC|:	2\
> \#|Arbs|:	1\
> \#Expanded length:	1\
> \#Kirchhoff polynomial:	None\
> END	

which can then be used to initialize an empty expression of Kirchhoff polynomials.

```python
>>> expr = KirchExpr()
>>> expr.initializeKirchExprFromFile(resultsFileName + '.kpy')
>>> print expr.generateExpression(edgeAttribute='label', enumArbs=True)

((r2+r3))/(r1)
```

## Manipulation of expressions of Kirchhoff polynomials

We demonstrate the manipulation capabilities of *KirchPy* on a model of the PHGS catalytic cycle from [2]. 

As is customary we first import the relevant modules and initialize the model.

```python
>>> from kirchpy.kirchexpr import *
>>> import kirchpy.common as cmn
>>> inputFileName = 'models/coxEdges.txt'
>>> separator     = '\t'
>>> arborescecnes = 'in'
>>> kpyFileName   = cmn.genKirchPyInputFromFile(inputFileName, separator=separator, arbs=arborescecnes)
>>> G = KirchMDigraph('G')
>>> G.initializeKirchExprFromFile(kpyFileName)
```

With the following commands we check the size (number of vertices), number of edges, number of strongly connected components, and complexity (number of arborescences/spanning trees) of the model directed graph:

```python
>>> print G.size(), G.getNumberOfEdges(), G.getNumberOfSCCs()

30 118 1

>>> print '{0:,}'.format(G.countArbs())

24,509,831,643,137,316
```

The model is of moderate size but has quadrillions of arborescences/spanning trees.

### Graph operations

Operations such as rooting, edge deletion and contraction, and vertex deletion can be applied to directed graphs.

```python
>>> G1 = G.copy()
>>> G1.rootAt('O')
>>> G1.deleteVertices('O')
>>> edgeDel = tuple(reversed(('E25','E26'))) # the edge has to be reversed since the internal representation of directed graphs is w.r.t. out-arborescences/out-spanning trees and model G is analysed w.r.t. in-arborescences/in-spanning trees
>>> G1.deleteEdges(edgeDel)
>>> edgeContr = tuple(reversed(('E21','E7')))
>>> G1.contractEdges(edgeContr)
>>> edgeDelContr = tuple(reversed(('E3','E1')))
>>> G1 = G1.edgeDeletionContraction(edgeDelContr).primeFactoriseInPlace()
>>> print G1.generateExpression()

K4(G35,G57,G9)*K2(G1,G10,G11,G12,G13,G14,G15,G16,G17,G18,G2,G20,G21,G22,G23,G24,G25,G26,G27,G28,G29,G3,G30,G31,G32,G33,G34,G37,G38,G39,G4,G40,G41,G42,G43,G44,G46,G47,G49,G5,G50,G51,G52,G53,G54,G55,G56,G59,G6,G61,G62,G65,G67,G7,G70,G73,G75,G76,G77,G78,G79,G8,G80,G81,G82,G83)*K6(G48)*K7(G60)*K10(G85)*K8(G64,G69,G72,G74,G87)*K9(G63)*K5(G66)*K3(G86)+K1(G0)*K4(G35,G57,G9)*K0(G1,G10,G11,G12,G13,G14,G16,G17,G18,G2,G20,G21,G22,G23,G24,G25,G26,G27,G28,G29,G3,G30,G31,G32,G33,G34,G37,G38,G39,G4,G40,G41,G42,G43,G44,G46,G47,G49,G5,G50,G51,G52,G53,G54,G55,G56,G59,G6,G61,G62,G65,G67,G7,G73,G75,G76,G77,G78,G79,G8,G80,G81,G82,G83)*K6(G48)*K7(G60)*K10(G85)*K8(G64,G69,G72,G74,G87)*K9(G63)*K5(G66)*K3(G86)
```

### Formulating expressions of Kirchhoff polynomials

The ratio between the steady states of species `'E21'` and `'E17'` in the open model `G` can be expressed as:

```python
>>> GE21 = G.copy()
>>> GE21.rootAt('E21')
>>> print '{0:,}'.format(GE21.countArbs()), '{0:,}'.format(GE21.expandedLengthKirchPol())

9,347,358,507,520 280,420,755,225,601

>>> GE17 = G.copy()
>>> GE17.rootAt('E17')
>>> print '{0:,}'.format(GE17.countArbs()), '{0:,}'.format(GE17.expandedLengthKirchPol())

2,336,839,626,880 70,105,188,806,401

>>> ratioE21E17 = FRAC(GE21, GE17)
>>> ratioE21E17Simpl = ratioE21E17.simplify()
>>> print ratioE21E17Simpl.generateExpression(enumArbs=True, edgeAttribute='label')

(k3*O2^2+k5*RC+kdeg+k15)/k13
```

We observe that despite the numerous arborescences/spanning trees of the directed graphs taking part in the numerator and denominator, the ratio can be simplified considerably in an instant.

Derivatives and integrals with respect to an edge label can also be formulated and simplified.

```python
>>> deriv = DERIV(GE21,INDET('G20'))
>>> print deriv.generateExpression()

diff(K0(G0,G1,G10,G100,G101,G102,G103,G104,G105,G106,G107,G109,G11,G110,G111,G112,G113,G114,G115,G116,G117,G12,G13,G14,G15,G16,G17,G18,G2,G20,G21,G22,G23,G24,G25,G26,G27,G28,G29,G3,G30,G31,G32,G33,G34,G35,G37,G38,G39,G4,G40,G41,G42,G43,G44,G46,G47,G48,G49,G5,G50,G51,G52,G53,G54,G55,G56,G57,G58,G59,G6,G60,G61,G62,G63,G64,G65,G66,G67,G68,G69,G7,G70,G71,G72,G73,G74,G75,G76,G77,G78,G79,G8,G80,G81,G82,G83,G85,G86,G87,G88,G89,G9,G90,G91,G92,G93,G94,G95,G96,G97,G98,G99),G20)

>>> print deriv.simplify().generateExpression()

K0(G0,G1,G10,G100,G101,G102,G103,G106,G107,G12,G13,G14,G15,G16,G17,G18,G2,G21,G22,G23,G24,G25,G26,G27,G28,G29,G3,G30,G31,G32,G33,G34,G37,G38,G39,G4,G40,G41,G42,G43,G44,G46,G47,G49,G5,G50,G51,G52,G53,G55,G56,G59,G6,G61,G65,G67,G7,G70,G73,G75,G76,G77,G78,G79,G8,G80,G81,G82,G83,G89,G90,G92,G93,G94,G95,G96,G97,G98,G99)*K4(G111,G112,G113,G114,G115,G63,G64,G66,G68,G69,G71,G72,G74,G86,G87)*K2(G104,G35,G57,G9)*K1(G117)*K5(G116)*K3(G105,G109,G110,G48,G58,G60,G85)

>>> integral = INTEGRAL(GE21,INDET('G20')) 
>>> print integral.generateExpression()

integrate(K0(G0,G1,G10,G100,G101,G102,G103,G104,G105,G106,G107,G109,G11,G110,G111,G112,G113,G114,G115,G116,G117,G12,G13,G14,G15,G16,G17,G18,G2,G20,G21,G22,G23,G24,G25,G26,G27,G28,G29,G3,G30,G31,G32,G33,G34,G35,G37,G38,G39,G4,G40,G41,G42,G43,G44,G46,G47,G48,G49,G5,G50,G51,G52,G53,G54,G55,G56,G57,G58,G59,G6,G60,G61,G62,G63,G64,G65,G66,G67,G68,G69,G7,G70,G71,G72,G73,G74,G75,G76,G77,G78,G79,G8,G80,G81,G82,G83,G85,G86,G87,G88,G89,G9,G90,G91,G92,G93,G94,G95,G96,G97,G98,G99),G20)

>>> print integral.simplify().generateExpression()

(G20*K0(G0,G1,G10,G100,G101,G102,G103,G106,G107,G11,G12,G13,G14,G15,G16,G17,G18,G2,G20/2,G21,G22,G23,G24,G25,G26,G27,G28,G29,G3,G30,G31,G32,G33,G34,G37,G38,G39,G4,G40,G41,G42,G43,G44,G46,G47,G49,G5,G50,G51,G52,G53,G54,G55,G56,G59,G6,G61,G65,G67,G7,G70,G73,G75,G76,G77,G78,G79,G8,G80,G81,G82,G83,G89,G90,G91,G92,G93,G94,G95,G96,G97,G98,G99)+Constpsi474)*K4(G111,G112,G113,G114,G115,G63,G64,G66,G68,G69,G71,G72,G74,G86,G87)*K2(G104,G35,G57,G9)*K1(G117)*K5(G116)*K3(G105,G109,G110,G48,G58,G60,G85)
```

Note that the variable for which differentiation and integration is applied has to be unique. Further, simplification needs to be done with respect to the edge label to which the variable corresponds.  In the example we have used the automatically generated edge labels.  `Constpsi474` is an integration constant.

The following expression illustrates the arithmetic operations implemented in *KirchPy*:

```python
>>> gr1 = integral.children[0].children[2] # a directed graph extracted from the expression 
>>> gr2 = integral.children[0].children[3]
>>> expr = INTEGRAL(
                	POWER(
                       	 ADD([
                            ZERO(),
                            DERIV(
                                FRAC(
                                    MUL([
                                        CONST('c'), 
                                        ADD([
                                            gr1, 
                                            NEG(gr1)
                                            ]),
                                        gr1
                                        ]), 
                                    gr2),
                                INDET('G116')
                                )
                            ]),
                        FRAC(
                            UNIT(),
                             CONST('2')
                             )
                     ),
                 	INDET('G85')
                 	)
                 
>>> print expr.generateExpression()

integrate(((0+diff((c*(K1(G105,G109,G110,G48,G58,G60,G85)+((-(K1(G105,G109,G110,G48,G58,G60,G85)))))*K1(G105,G109,G110,G48,G58,G60,G85))/(K0(G116)),G116)))**(1/2),G85)

>>> print expr.simplify().generateExpression()

0
```

## Generation of Kirchhoff polynomials

We demonstrate the generation capabilities of *KirchPy* on a graph of the protein interactions involved in cardiac tension development from [3].

Again, we first import the relevant modules and initialize the model.

```python
>>> from kirchpy.kirchexpr import *
>>> import kirchpy.common as cmn
>>> inputFileName = 'models/actinmyosin.txt'
>>> separator     = '\t'
>>> arborescecnes = 'in'
>>> kpyFileName   = cmn.genKirchPyInputFromFile(inputFileName, separator=separator, arbs=arborescecnes)
>>> G = KirchMDigraph('G')
>>> G.initializeKirchExprFromFile(kpyFileName)
```

We can choose which edge deletion-contraction heuristic to use during the generation by specifying a tuple of four integers (default heuristic is (1,0,0,0)).


- i.) Edges E'(G):
  - 0. A single randomly selected edge e, E'(G)={e}.
  - 1. All edges, E'(G)=E(G).
  - 2. Edges participating in the longest simple cycle.
  - 3. The |E(G)|/n, n=3 edges participating in the largest number of simple cycles.
- ii.) Branch:
  - 0. Edge deleted directed graph, $`G\setminus e`$.
  - 1. Edge contracted directed graph, $`G/e`$.
  - 2. Edge deleted and edge contracted directed graph, $`(G\setminus e,G/e)`$.
- iii.) Components:
  - 0. Strongly connected components.
  - 1. Prime components.
- iv.) Optimality criterion:
  - 0. Largest number of components.
  - 1. Largest component (in terms of number of vertices) is smallest.
  - 2. Largest component (in terms of number of edges) is smallest.
  - 3. Smallest total complexity (number of arborescences/spanning trees) of the components. 
  - 4. Smallest total complexity with the largest (same) number of components.
  - 5. Largest number of components with the (same) smallest total complexity.

```python
>>> print G.expandedLengthKirchPol()  
  
3561 
   
>>> l1 = G.enumerateArbs(heuristic=(1,1,1,3)).getCompressedLength()  
>>> l2 = G.enumerateArbs(heuristic=(3,0,0,3)).getCompressedLength()  
>>> print l1, l2  
  
542 243   
```

We observe that the Kirchhoff polynomial in expanded representation is considerably lengthier than in the compressed representation for the two choices of heuristic.

There are three choices for generation algorithms:

- `'recursive'`-- recursive compression algorithm $`C_R`$
- `'implicit_full'`-- iterative compression algorithm $`C_I`$ with substitutions applied (outputs a single Kirchhoff polynomial)
- `'implicit'`-- iterative compression algorithm $`C_I`$ without applying the substitutions (outputs a Kirchhoff polynomial in `CompactKirchPol` form)

```python
>>> a1 = G.enumerateArbs(algorithm='recursive',heuristic=(3,0,0,3)).getCompressedLength()  
>>> a2 = G.enumerateArbs(algorithm='implicit_full',heuristic=(3,0,0,3)).getCompressedLength()  
>>> a3 = G.enumerateArbs(algorithm='implicit',heuristic=(3,0,0,3)).getCompressedLength()  
>>> print a1, a2, a3  

243 243 142
```

 The form of the Kirchhoff polynomials generated by  $`C_I`$ differs depending on whether substitutions were applied or not.

```python
>>> print G.enumerateArbs(algorithm='implicit_full',heuristic=(3,0,0,3),edgeAttribute='label').generateExpression()  

(((k6+km5+k13)*(k7+k11)+km6*(km5+k13))*(((km1+k2)*(km8+k9)+k1*k2)*k10+k8*((km1+k2)*k9+k1*k2))*k12*k14*(km4*(k3+km3)+k4*k3)+k5*((km6+k7+k11)*(((km1+k2)*(km8+k9)+k1*k2)*k10+k8*((km1+k2)*k9+k1*k2))*k12*k14*(k13*km3+k3*(k4+k13))+k6*((k4+km3)*(k7+k11)*(((km1+k2)*(km8+k9)+k1*k2)*k10+k8*((km1+k2)*k9+k1*k2))*k12*k14+k3*((k7+k11)*(((km1+k2)*(km8+k9)+k1*k2)*k10+k8*((km1+k2)*k9+k1*k2))*k12*k14+k4*((((km1+k2)*(km8+k9)+k1*k2)*k10+k8*((km1+k2)*k9+k1*k2))*(k14*k11+k12*(k14+k11))+k7*(((km1+k2)*(km8+k9)+k1*k2)*(k12*k14+k10*(k12+k14))+k8*k12*((km1+k2)*(k9+k14)+k1*k14)))))))

>>> print G.enumerateArbs(algorithm='implicit',heuristic=(3,0,0,3),edgeAttribute='label').generateExpression()  

start=(((k6+km5+k13)*(k7+k11)+km6*(km5+k13))*psi2280*k12*k14*(km4*(k3+km3)+k4*k3)+k5*((km6+k7+k11)*psi2280*k12*k14*(k13*km3+k3*(k4+k13))+k6*((k4+km3)*(k7+k11)*psi2280*k12*k14+k3*((k7+k11)*psi2280*k12*k14+k4*(psi2280*(k14*k11+k12*(k14+k11))+k7*(psi2288*(k12*k14+k10*(k12+k14))+k8*k12*((km1+k2)*(k9+k14)+k1*k14))))))),psi2280=(psi2288*k10+k8*((km1+k2)*k9+k1*k2)),psi2288=((km1+k2)*(km8+k9)+k1*k2)
```

## Additional information

Additional information about biological applications and the underlying theory can be found in [4], [5], and [6].

## Authors

© 2017, ETH Zurich, Pencho Yordanov

	Pencho Yordanov <yordanov.pencho@gmail.com>
## References

[1]	Tomida, T., Hirose, K., Takizawa, A., Shibasaki, F., Iino, M. (2003). NFAT functions as a working memory of Ca2+ signals in decoding Ca2+ oscillation. The EMBO Journal, 22(15), 3825-3832. https://doi.org/10.1093/emboj/cdg381

[2]	Goltsov, A., Lebedeva, G., Humphery-Smith, I., Goltsov, G., Demin, O., & Goryanin, I. (2010). In Silico Screening of Nonsteroidal Anti-inammatory Drugs and their Combined Action on Prostaglandin H Synthase-1. Pharmaceuticals 3, 2059-2081. https://doi.org/10.3390/ph3072059

[3] Sachse, F. B., Glänzel, K. G., and Seemann, G. (2003). Modeling of protein interactions involved in cardiac tension development. International Journal of Bifurcation and Chaos, 13(12), 3561-3578. http://www.worldscientific.com/doi/abs/10.1142/S0218127403008855

[4]	Yordanov, P.  and Stelling, J. (2020). Efficient manipulation and generation of Kirchhoff polynomials for the analysis of non-equilibrium biochemical reaction networks. In J. R. Soc. Interface. https://doi.org/10.1098/rsif.2019.0828

[5] Mihalák, M., P. Uznański, and Yordanov, P. (2016). Prime factorization of the Kirchhoff polynomial: Compact enumeration of arborescences. In ANALCO, pages 93–105. https://doi.org/10.1137/1.9781611974324.10

[6] Yordanov, P.  and Stelling, J. (2018). Steady-State Differential Dose Response in Biological Systems. In Biophysical Journal. https://doi.org/10.1016/j.bpj.2017.11.3780

[1]: https://doi.org/10.1093/emboj/cdg381	"Tomida, T., Hirose, K., Takizawa, A., Shibasaki, F., Iino, M. (2003). NFAT functions as a working memory of Ca2+ signals in decoding Ca2+ oscillation. The EMBO Journal, 22(15), 3825-3832."
[2]: https://doi.org/10.3390/ph3072059	"Goltsov, A., Lebedeva, G., Humphery-Smith, I., Goltsov, G., Demin, O., & Goryanin, I. (2010). In Silico Screening of Nonsteroidal Anti-inammatory Drugs and their Combined Action on Prostaglandin H Synthase-1. Pharmaceuticals 3, 2059-2081.  "
[3]: http://www.worldscientific.com/doi/abs/10.1142/S0218127403008855	"Sachse, F. B., Glänzel, K. G., and Seemann, G. (2003). Modeling of protein interactions involved in cardiac tension development. International Journal of Bifurcation and Chaos, 13(12), 3561-3578."
[4]: https://doi.org/10.1098/rsif.2019.0828	"Yordanov, P.  and Stelling, J. (2020). Efficient manipulation and generation of Kirchhoff polynomials for the analysis of non-equilibrium biochemical reaction networks. In J. R. Soc. Interface."
[5]: https://doi.org/10.1137/1.9781611974324.10	" Mihalák, M., P. Uznański, and Yordanov, P. (2016). Prime factorization of the Kirchhoff polynomial: Compact enumeration of arborescences. In ANALCO, pages 93–105."
[6]: https://doi.org/10.1016/j.bpj.2017.11.3780	"Yordanov, P.  and Stelling, J. (2018). Steady-State Differential Dose Response in Biological Systems. In Biophysical Journal."