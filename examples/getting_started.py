# -*- coding: utf-8 -*-
"""
© 2017, ETH Zurich, Pencho Yordanov
Getting started with KirchPy.
"""
from kirchpy.kirchexpr import *
import kirchpy.common as cmn

inputFileName = 'models/NFAT.txt'
separator     = '\t'
arborescecnes = 'in'
kpyFileName   = cmn.genKirchPyInputFromFile(inputFileName, separator=separator, arbs=arborescecnes)

G = KirchMDigraph('G')
G.initializeKirchExprFromFile(kpyFileName)

Gnum = G.copy()
Gnum.rootAt('Nn')

xSSNn = MUL([FRAC(Gnum, G), INDET('xT', name='total conserved')]) # Note that the total conserved xT is implemented as a variable/indeterminate

print xSSNn.generateExpression(edgeAttribute='label')

xSSNn.primeFactoriseInPlace()
print xSSNn.generateExpression(edgeAttribute='label')

xSSNnSimpl = xSSNn.simplify()
print xSSNnSimpl.generateExpression(edgeAttribute='label')

print xSSNn.generateExpression(enumArbs=True)

print xSSNn.generateExpression(edgeAttribute='label', enumArbs=True)

xSSNn.enumArbsInPlace(edgeAttribute='label')
print xSSNn.generateExpression()

# ratio
GnumNcs = G.copy()
GnumNcs.rootAt('Nc*')
xSSNcs = MUL([FRAC(GnumNcs, G), INDET('xT', name='total conserved')])
GnumNc = G.copy()
GnumNc.rootAt('Nc')
xSSNc = MUL([FRAC(GnumNc, G), INDET('xT', name='total conserved')])
ratio = FRAC(xSSNcs, xSSNc)
ratioOriginal = ratio.copy() # keep a copy of the expression before manipulating it (details later)

ratio = ratio.simplify()
print ratio.generateExpression(edgeAttribute='label', enumArbs=True)
print ratioOriginal.generateExpression(edgeAttribute='label',enumArbs=True)
print ratio.isEqual(ratioOriginal)

print ratio.isEqual(ratioOriginal.enumArbsInPlace())
print ratio.isEqual(ratioOriginal.enumArbsInPlace(), generateKirchhoffPols=True)


resultsFileName = 'models/results'
ratio.writeKirchExprToFile(resultsFileName, edgeAttribute='label')
# read expression
expr = KirchExpr()
expr.initializeKirchExprFromFile(resultsFileName + '.kpy')
print expr.generateExpression(edgeAttribute='label', enumArbs=True)