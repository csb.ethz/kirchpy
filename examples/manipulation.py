# -*- coding: utf-8 -*-
"""
© 2017, ETH Zurich, Pencho Yordanov
Manipulating expressions of Kirchhoff polynomials with KirchPy.
"""
from kirchpy.kirchexpr import *
import kirchpy.common as cmn

inputFileName = 'models/coxEdges.txt'
separator     = '\t'
arborescecnes = 'in'
kpyFileName   = cmn.genKirchPyInputFromFile(inputFileName, separator=separator, arbs=arborescecnes)
G = KirchMDigraph('G')
G.initializeKirchExprFromFile(kpyFileName)

print G.size(), G.getNumberOfEdges(), G.getNumberOfSCCs()
print '{0:,}'.format(G.countArbs())

# Graph operations
G1 = G.copy()
G1.rootAt('O')
G1.deleteVertices('O')
print G1.generateExpression()
edgeDel = tuple(reversed(('E25','E26'))) # the edge has to be reversed since the internal representation of directed graphs is w.r.t. out-arborescences/out-spanning trees and model G is analysed w.r.t. in-arborescences/in-spanning trees
G1.deleteEdges(edgeDel)
edgeContr = tuple(reversed(('E21','E7')))
G1.contractEdges(edgeContr)
edgeDelContr = tuple(reversed(('E3','E1')))
G1 = G1.edgeDeletionContraction(edgeDelContr).primeFactoriseInPlace()
print G1.generateExpression()


# Expressions of Kirchhoff polynomials

# ratio
GE21 = G.copy()
GE21.rootAt('E21')
print '{0:,}'.format(GE21.countArbs()), '{0:,}'.format(GE21.expandedLengthKirchPol())
GE17 = G.copy()
GE17.rootAt('E17')
print '{0:,}'.format(GE17.countArbs()), '{0:,}'.format(GE17.expandedLengthKirchPol())
ratioE21E17 = FRAC(GE21, GE17)
ratioE21E17Simpl = ratioE21E17.simplify()
print ratioE21E17Simpl.generateExpression(enumArbs=True, edgeAttribute='label')

# calculus
deriv = DERIV(GE21,INDET('G20')) # NOTE: the variable wrt which differentiation is applied has to be unique and simplification needs to be done wrt the edge label to which the variables corresponds (the automatically generated labels are used in this example)
print deriv.generateExpression()
print deriv.simplify().generateExpression()

integral = INTEGRAL(GE21,INDET('G20')) 
print integral.generateExpression()
print integral.simplify().generateExpression()

gr1 = integral.children[0].children[2] # a directed graph extracted from the expression 
gr2 = integral.children[0].children[3]

expr = INTEGRAL(
                POWER(
                        ADD([
                            ZERO(),
                            DERIV(
                                FRAC(
                                    MUL([
                                        CONST('c'), 
                                        ADD([ 
                                            gr1, 
                                            NEG(gr1)
                                            ]),
                                        gr1
                                        ]), 
                                    gr2),
                                INDET('G116')
                                )
                            ]),
                        FRAC(
                            UNIT(),
                             CONST('2')
                             )
                     ),
                 INDET('G85')
                 )
                 
print expr.generateExpression()
print expr.simplify().generateExpression()