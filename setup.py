#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""KirchPy
@ author: Pencho Yordanov, 05.2017
"""

from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='kirchpy',
      version='0.1.1',
      description='Efficient manipulation and compact generation of Kirchhoff polynomials',
      long_description=readme(),
      keywords='Kirchhoff polynomials spanning trees arborescences enumeration Matrix Tree Theorem',
      url='https://gitlab.com/csb.ethz/KirchPy',
      author='Pencho Yordanov',
      author_email='yordanov.pencho@gmail.com',
      license='LICENSE.txt',
      packages=['kirchpy'],
      install_requires=[
          "markdown",
          "NetworkX >= 1.11.0,<2",
          "sympy >= 1.0.0"
      ],
      classifiers=[
        "Programming Language :: Python :: 2.7",
      ],
      include_package_data=True,
      )